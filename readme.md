# Morgen! #

* Achtung! Bis Freitag andere Sprechzeiten:
* Achtung! Bis Freitag andere Sprechzeiten:
* Achtung! Bis Freitag andere Sprechzeiten:

## Wissensvermittlung ##
* 09:45 - 10:30 Uhr
* 12:00 - 12:30 Uhr

## Sprechstunden ##
* 13:30 - 15:00 Uhr (per Chat)
* 15:15 - 16:45 Uhr (im Betreuungsraum)

# Aufgaben #

* Prüfungsvorbereitung

# Allgemein #
* Kursinhalte: 	https://bitbucket.org/ccjavad/wcd_088432.git
* Prüfung:		28.10.2016
* Klausur:		07.10.2016

# Fehler in den Büchern
* HeadFirst: http://www.oreilly.com/catalog/errata.csp?isbn=9780596516680
* Companion: http://www.garnerpress.com/catalogue/BK0340/errata.html

# Git mit Netbeans #
* Repo clonen: Team->Git->Clone (oder Team->Remote->Clone)
* Repository Browser öffnen: Team -> Repository -> Repository Browser
* Repo aktualisieren: Im Repository Browser Rechtsclick auf das Repo -> Remote -> Pull

# Pausen: #
* 09:30 - 09:45  
* 11:15 - 11:30  
* 13:00 - 13:30  

* AJAX Bsp.
* LDAP: http://www.zflexsoftware.com/index.php/pages/free-online-ldap
* env-entry und GlassFish
