package ctrl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Memory;

public class ServletController extends HttpServlet {
    
    private List<String> listImageNames;

    @Override
    public void init() throws ServletException {
        
        ServletContext context = getServletContext();
        
        String imagesDirName = context.getRealPath("/images");
        
        File imagesDir = new File(imagesDirName);
        
        String[] imageNames = imagesDir.list();

        listImageNames = Arrays.asList(imageNames);
        
        listImageNames = new ArrayList<>(listImageNames);
        listImageNames.remove("cover.png");
        
        listImageNames = Collections.unmodifiableList(listImageNames);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        //0. User-Wunsch erkennen
        //1. Modell vorbereiten
        //2. View aktivieren
        
        
        String requestUri = request.getRequestURI();
        
        if(requestUri.contains("/newGame")) {
            
            String paramLevel = request.getParameter("level");
            
            Memory.Level level = Memory.Level.valueOf(paramLevel.toUpperCase());
            
            Memory game = new Memory(level, listImageNames);
            
            HttpSession session = request.getSession();
            
            session.setAttribute("game", game);
            
        } else if( requestUri.contains("/openImage") ) {
            
            String indexParam = request.getParameter("index");
            
            int index = Integer.parseInt(indexParam);
            
            HttpSession session = request.getSession();
            
            Memory game = (Memory) session.getAttribute("game");
            if(game!=null) {
                game.openImage(index);
            }
        } 
//        else if( requestUri.contains("/") || requestUri.contains("/welcome") ) {
//            //Nichts tun???
//        } else {
//            throw new UnsupportedOperationException("Request not supported: " + requestUri);
//        }
        
//        request.setAttribute("game", game);
        
//        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view");
        RequestDispatcher rd = request.getRequestDispatcher("/view.jsp");
        rd.forward(request, response);
    }
    
    
    
}
