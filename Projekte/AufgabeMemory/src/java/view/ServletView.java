package view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Memory;
import model.MemoryImage;

/**
 *
 * @author apatrin
 */
public class ServletView extends HttpServlet {

    private String getNewGameUrl(String level, HttpServletResponse response) {
        String url = "newGame?level=" + level;
        
        return response.encodeURL(url);
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Memory</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Memory</h1>");
            
            out.println("Neues Spiel starten:");
            
            HttpSession session = request.getSession();
            
            String url = getNewGameUrl("easy", response);
            out.println("<a href=\"" + url + "\">leicht</a>");
            
            url = getNewGameUrl("normal", response);
            out.println("<a href=\"" + url + "\">normal</a>");
            
            url = getNewGameUrl("hard", response);
            out.println("<a href=\"" + url + "\">schwer</a>");
            
            out.println("<hr/>");
            
            Memory game = (Memory)session.getAttribute("game");
            
            if(game!=null) {
                List<MemoryImage> images = game.getImages();

                for (int i = 0; i < images.size(); i++) {
                    MemoryImage img = images.get(i);

                    if( img.isOpen() ) {
                        String imgName = img.getName();
                        out.println("<img src=\"images/" + imgName + "\" width=\"50\"/>");
                    } else {
                        
                        url = "openImage?index=" + i;
                        url = response.encodeURL(url);
                        
                        out.println("<a href=\"" + url + "\"><img src=\"images/cover.png\" width=\"50\"/></a>");
                    }
                }
            }
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
