<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Memory</title>
    </head>
    <body>
        <h1>Memory</h1>
        
        Neues Spiel starten:
        
        <c:url var="targetUrl"  value="newGame?level=easy"/>
        <a href="${targetUrl}">leicht</a>
        
        <c:url var="targetUrl"  value="newGame?level=normal"/>
        <a href="${targetUrl}">normal</a>
        
        <c:url var="targetUrl"  value="newGame?level=hard"/>
        <a href="${targetUrl}">schwer</a>
        
        <hr/>
        
        <c:forEach items="${game.images}" var="img" varStatus="status">
            <c:choose>
                <c:when test="${img.open}">
                    <img src="images/${img.name}" width="50"/>
                </c:when>
                <c:otherwise>
                    <c:url var="targetUrl" value="openImage?index=${status.count-1}"/>
                    <a href="${targetUrl}"><img src="images/cover.png" width="50"/></a>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        
    </body>
</html>
