<%!
    private String getNewGameUrl(String level, HttpServletResponse response) {
        String url = "newGame?level=" + level;
        
        return response.encodeURL(url);
    }    
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.*" %>
<%@page import="java.util.List" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Memory</title>
    </head>
    <body>
        <h1>Memory</h1>
        
        Neues Spiel starten:
        
        <%
            String url = getNewGameUrl("easy", response);
        %>
        <a href="<%=url%>">leicht</a>
            
        <%    
            url = getNewGameUrl("normal", response);
        %>
        <a href="<%=url%>">normal</a>

        <%
            url = getNewGameUrl("hard", response);
        %>
        <a href="<%=url%>">schwer</a>
            
        <hr/>
            
        <%    
            Memory game = (Memory)session.getAttribute("game");
            
            if(game!=null) {
                List<MemoryImage> images = game.getImages();

                for (int i = 0; i < images.size(); i++) {
                    MemoryImage img = images.get(i);

                    if( img.isOpen() ) {
                        String imgName = img.getName();
        %>
                        <img src="images/<%= imgName %>" width="50"/>
        <%
                    } else {
                        
                        url = "openImage?index=" + i;
                        url = response.encodeURL(url);
        %>
                        <a href="<%=url%>"><img src="images/cover.png" width="50"/></a>
        <%                
                    }
                }
            }
        %>
        
    </body>
</html>
