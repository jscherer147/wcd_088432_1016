package svts;

import ds.Comparators;
import ds.Stadt;
import ds.db.DatabaseAccess;
import ds.db.MySqlAccess;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletStaedte extends HttpServlet { //implements SingleThreadModel {

    private List<Stadt> staedte;

    @Override
    public void init() throws ServletException {
        try {
            DatabaseAccess dbAccess = new MySqlAccess();
        
            staedte = dbAccess.selectAll();

        } catch(SQLException | ClassNotFoundException | IOException e) {
            throw new ServletException(e);
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String sort = request.getParameter("sort");
        
        Comparator<Stadt> cmp = null;
        
        if("name".equals(sort)) {
            cmp = Comparators.CMP_NAME;
        } else if("land".equals(sort)) {
            cmp = Comparators.CMP_LAND;
        } else if("einwohner".equals(sort)) {
            cmp = Comparators.CMP_EINWOHNER;
        } 
        
        PrintWriter out = response.getWriter();
        
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>Deutsche Städte</h1>");
        out.println("<table>");
        out.println("<tr>");
        out.println("<th>Nr.</th>");
        out.println("<th><a href=\"cities?sort=name\">Name</a></th>");
        out.println("<th><a href=\"cities?sort=land\">Land</a></th>");
        out.println("<th><a href=\"cities?sort=einwohner\">Einwohner</a></th>");
        out.println("</tr>");

        synchronized(staedte) {
            if(cmp!=null) {
                Collections.sort(staedte, cmp);
            }

            for (int i = 0; i < staedte.size(); i++) {
                Stadt s = staedte.get(i);

                out.println("<tr>");
                out.println("<td>" + (i+1) + "</td>");
                out.println("<td>" + s.getName() + "</td>");
                out.println("<td>" + s.getLand()+ "</td>");
                out.println("<td>" + s.getEinwohner()+ "</td>");

                out.println("</tr>");
            }
        }
        
        out.println("</table>");
        out.println("</body>");
        out.println("</html>");

    }
}
