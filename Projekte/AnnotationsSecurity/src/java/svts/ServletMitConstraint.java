package svts;

import java.io.IOException;
import java.io.PrintWriter;
import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
    <security-role>
        <role-name>regUser</role-name>
    </security-role>
    <security-role>
        <role-name>regAdmin</role-name>
    </security-role>


für ein Servlet:
@DeclareRoles( {"regUser", "regAdmin"} )

*/

/*
    Geht nut im DD:

    <login-config>
        <auth-method>BASIC</auth-method>
    </login-config>
*/


/*    
    <security-constraint>
        <web-resource-collection>
            <web-resource-name>CollAll</web-resource-name>
            <url-pattern>/user.jsp</url-pattern>
            <http-method>GET</http-method>
            <http-method>POST</http-method>
        </web-resource-collection>
        
        <auth-constraint>
            <role-name>regUser</role-name>
        </auth-constraint>
    </security-constraint>
*/

@DeclareRoles( {"regUser", "regAdmin"} )

@ServletSecurity( 
    httpMethodConstraints = {
        @HttpMethodConstraint( value = "GET", rolesAllowed = "regUser" ),
        @HttpMethodConstraint( value = "POST", rolesAllowed = "regUser" ),
    }
)

@WebServlet(name = "ServletMitConstraint", 
        urlPatterns = {"/servletMitConstraint"})
public class ServletMitConstraint extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletMitConstraint</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServletMitConstraint at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
