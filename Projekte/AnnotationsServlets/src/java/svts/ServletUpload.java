package svts;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@MultipartConfig (
    fileSizeThreshold = 100_000,
    location = "C:\\Windows\\Temp",
    maxFileSize = 100_000_000_000_000L,
    maxRequestSize = 100_000_000_000_000L
)
@WebServlet(
    name = "ServletUpload", 
    urlPatterns = {"/upload"},
    initParams = @WebInitParam(name = "targetPath", value = "C:\\Users\\apatrin\\Desktop\\target")
)
public class ServletUpload extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletUpload</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Upload</h1>");

//        Part partFile1 = request.getPart("file1");
            Collection<Part> allParts = request.getParts();

            for (Part part : allParts) {
                String result = process(part);
                out.print(result + "<br/>");
            }

            out.println("</body>");
            out.println("</html>");
        }
    }
    
    private String process(Part part) throws IOException {
        if( part.getSize() == 0 ) {
            return "Parameter " + part.getName() + " nicht gesendet";
        }
        
        String targetPathName = getInitParameter("targetPath");
        Path target = Paths.get(targetPathName, part.getSubmittedFileName());

        try( InputStream is = part.getInputStream() ) {
            Files.copy(is, target, StandardCopyOption.REPLACE_EXISTING);
        }
        
        return "Hochgeladen: " + part.getSubmittedFileName();
    }

}
