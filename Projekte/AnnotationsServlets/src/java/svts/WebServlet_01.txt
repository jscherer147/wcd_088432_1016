    <servlet>
        <servlet-name>ServletImDD</servlet-name>
        <servlet-class>svts.ServletImDD</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>ServletImDD</servlet-name>
        <url-pattern>/servletImDD</url-pattern>
    </servlet-mapping>


Ohne den Servlet-Namen:

//@WebServlet("/servletMitAnn")
// dasselbe:
//@WebServlet(value = "/servletMitAnn")
// dasselbe:
//@WebServlet(urlPatterns = "/servletMitAnn")


Url-Patterns und der Servlet-Name

@WebServlet(
    name = "MyServletName",
    urlPatterns = "/servletMitAnn"
)
