package listener;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class InitUserListExample implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        List<String> userList = new ArrayList<>();
        userList.add("tom");
        userList.add("jerry");
        
        ServletContext context = sce.getServletContext();
        context.setAttribute("userList", userList);
        
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

    
    
}
