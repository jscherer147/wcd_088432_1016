package svts;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ServletUserList", urlPatterns = {"/userList"})
public class ServletUserList extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try (PrintWriter out = response.getWriter()) {
            
            List<String> userList = (List)getServletContext().getAttribute("userList");
            
            out.println(userList);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        List<String> userList = (List)getServletContext().getAttribute("userList");
        
        String userName = req.getParameter("userName");
        
        if( !userList.contains(userName) ) {
            userList.add(userName);
        }
        
    }
    
}
