<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <script>
            function updateUserList() {
                
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET", "userList", false);
                
                xmlhttp.onreadystatechange = function() {
                    if( xmlhttp.readyState === 4 && xmlhttp.status === 200 ) {
                        
                        var outUserList = document.getElementById("id-user-list");
                        outUserList.innerHTML = xmlhttp.responseText;
                        
                    }
                };
                
                xmlhttp.send();
            }
        </script>
    </head>
    <body onload="setInterval(updateUserList, 1000)">

        <div id="id-user-list"></div>
        
    </body>
</html>
