<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <script>
            var count = 0;
            
            function increment() {
                count++;
                
                var countView = document.getElementById("id-count");
                countView.innerHTML = count;
            }
        </script>
    </head>
    
    <body onload=" setInterval(increment, 1000)">
        <h1>Count: <span id="id-count"></span></h1>
    </body>
</html>
