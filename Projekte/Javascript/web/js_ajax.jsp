<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <script>
            function sendAjaxRequest() {
                
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET", "simpleAjax", false);
                
                xmlhttp.onreadystatechange = function () {
                    if( xmlhttp.readyState === 4 && xmlhttp.status === 200 ) {
                        var resultView = document.getElementById("id-ajax-result");
                        resultView.innerHTML = xmlhttp.responseText;
                    }
                };
                
                xmlhttp.send(null);
            }
        </script>
    </head>
    <body>
        
        <p onclick="sendAjaxRequest()" >
            Click hier um AJAX Anfrage zu senden
        </p>
        
        <h1 id="id-ajax-result"></h1>
        
    </body>
</html>
