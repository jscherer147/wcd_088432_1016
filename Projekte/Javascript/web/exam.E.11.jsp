<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <%--        
        <script>
            MY_ARRAY = new Array();
            MY_ARRAY[0] = 'mo';
            MY_ARRAY[1] = 'di';
            MY_ARRAY[2] = 'mi';
        </script>
        --%>

        <%--
        <script>
            MY_ARRAY = new Array();
            <%
                String[] serverArray = { "mo", "di", "mi" };
                
                for(int i=0; i<serverArray.length; i++) {
                    out.print("MY_ARRAY[");
                    out.print(i);
                    out.print("] = '");
                    out.print(serverArray[i]);
                    out.println("';");
                }
            %>
        </script>
        --%>

        <%--
        <script>
            MY_ARRAY = new Array();
            <%
                String[] serverArray = { "mo", "di", "mi" };
                
                for(int i=0; i<serverArray.length; i++) {
            %>
                    MY_ARRAY[
            <%
                    out.print(i);
            %>
                    ] = '
            <%
                    out.print(serverArray[i]);
            %>
                    ';
            <%
                }
            %>
        </script>
        --%>
        
        <script>
            MY_ARRAY = new Array();
            <%
                String[] serverArray = { "mo", "di", "mi" };
                
                for(int i=0; i<serverArray.length; i++) {
            %>
                    MY_ARRAY[
            <%=i%>
                    ] = '
            <%= serverArray[i]%>
                    ';
            <%
                }
            %>
        </script>
        
        
    </head>
    
    <body>
        <h1>Hello World!</h1>
    </body>
</html>
