<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <script>
            function f1() {
                var mainOut = document.getElementById("id-main");
                mainOut.innerHTML = "Geladen";
            }
            
            function f2(text) {
                var mainOut = document.getElementById("id-main");
                mainOut.innerHTML = text;
            }
        </script>
    </head>
    
    <body onload="f1()">
        <h1 id="id-main">Hello World!</h1>
        
        <p onclick="f2('Neuer Text')">Click um den Text zu ändern</p>
        
    </body>
</html>
