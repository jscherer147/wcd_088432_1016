<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <c:if test="true">
            1. c:if. EL-Auswerten: ${1 + 2}<br/>
        </c:if>

        <c:if test="${null}">
            2. c:if <br/>
        </c:if>

        <c:if test="wahr">
            3. c:if <br/>
        </c:if>

        <c:if test="${2 - 1 < 3}">
            4. c:if <br/>
        </c:if>

        <c:if test="<%= 2 - 1 < 3%>">
            5. c:if <br/>
        </c:if>

        <hr/>

        <c:if test="${not empty nichtda}" var="erg">
            6. if. body. erg = ${erg} <br/>
        </c:if>

        6. if. nach dem Body. erg = ${erg} <br/>
        
        
        <c:if test="${true}" var="erg" scope="request">
            7. if. body. erg (request-scope) = ${requestScope.erg} <br/>
        </c:if>
        7. if. nach dem Body. erg (request-scope) = ${requestScope.erg} <br/>


    </body>
</html>
