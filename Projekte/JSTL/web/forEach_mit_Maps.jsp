<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <%
            
            Map<String, Object> m1 = new HashMap<>();
            
            Map.Entry<String, Object> element;
            
            Set< Map.Entry<String, Object> >  alleEntries = m1.entrySet();
            
            for(Map.Entry<String, Object> var : alleEntries ) {
                var.getKey();
                var.getValue();
            }
        %>
        
        <h3>Headers</h3>
        <table>
        <c:forEach items="${header}" var="entry">
            <tr>
                <td>${entry.key}</td>
                <td>${entry.value}</td>
            </tr>
        </c:forEach>
        </table>
        
        
        <hr/>
        <c:forEach items="${applicationScope}" var="entry">
            ${entry.key} <br/>
        </c:forEach>
        
        <hr/>
        <%
            pageContext.setAttribute("props", System.getProperties());
        %>
        
        <c:forEach items="${props}" var="entry">
            ${entry.key} <br/>
        </c:forEach>
    </body>
</html>
