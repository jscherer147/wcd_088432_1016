<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <c:url var="targetUrl" value="target.jsp"/>
        url: ${targetUrl} <br/>
        
        <c:url var="targetUrl" value="target.jsp">
            <c:param name="size" value="small"/>
        </c:url>
        url: ${targetUrl} <br/>
        
        <hr/>
        <c:url var="targetUrl" value="target.jsp?userName=Max Müller"/>
        url: ${targetUrl} <br/>
        
        <a href="${targetUrl}">test</a> <br/>
        
        <c:url var="targetUrl" value="target.jsp">
            <c:param name="userName" value="Max Müller"/>
        </c:url>
        url: ${targetUrl} <br/>
    </body>
</html>
