<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <%
            List<String> list = Arrays.asList("mo", "di", "mi");
            pageContext.setAttribute("wochentage", list);
        %>
        
        <c:forEach items="${wochentage}" var="t">
            ${t} 
        </c:forEach>
        
        
    </body>
</html>
