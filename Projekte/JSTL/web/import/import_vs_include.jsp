<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <h1>Interne Ressourcen einbinden</h1>
        <%@include file="line.jsp" %>
        <jsp:include page="line.jsp"/>
        <c:import url="line.jsp"/>
        
        <h1>Ressourcen aus einem Fremdcontext einbinden</h1>
        <c:import url="/otherLine.jsp" context="/ForeignContext"/>
        
        <h1>Externe Ressourcen einbinden</h1>
        <%--
        <c:import url="www.google.de" />
        --%>
        <c:import url="http://www.if-schleife.de/" var="htmlText" scope="page" />
        
        <c:if test="${not empty pageScope.htmlText}">
            Es gibt jetzt das Attribut 'htmlText' 
            mit dem Body der Antwort von if-schleife.de im page scope
        </c:if>
            <br/>
        
        <c:import url="http://www.if-schleife.de/" varReader="reader">
            reader im Body von import: ${reader} <br/>
        </c:import>
        reader ist NACH dem Body von import weg: ${reader} <br/>
        
    </body>
</html>
