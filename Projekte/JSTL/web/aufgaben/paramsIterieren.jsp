<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <c:url var="targetUrl" value="paramsIterieren.jsp">
            <c:param name="color" value="red"/>
            <c:param name="color" value="orange"/>
            <c:param name="color" value="blue"/>
            <c:param name="size" value="big"/>
            <c:param name="size" value="small"/>
        </c:url>

        <a href="${targetUrl}">sende Test-Parameter</a>
        
        <h3>Request-Parameter:</h3>
        
        <c:forEach items="${paramValues}" var="entry">
            
            <h3>${entry.key}</h3>
            
            <c:forEach items="${entry.value}" var="paramValue" varStatus="status">
                ${paramValue}${status.last ? "" : ","}
            </c:forEach>
            <br/>
            
        </c:forEach>
        
    </body>
</html>
