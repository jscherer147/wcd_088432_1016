<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <%--
            if(test)
        --%>
        <c:choose>
            <c:when test="false">
                1.1 when<br/>
            </c:when>
        </c:choose>
        
        <%--
            if(test)
            else
        --%>
        <c:choose>
            <c:when test="${1+1 > 100}">
                2.1 when<br/>
            </c:when>
            <c:otherwise>
                2. otherwise<br/>
            </c:otherwise>
        </c:choose>
        
        <%--
            if(test1)
            else if(test2)
        --%>
        <c:choose>
            <c:when test="${true}">
                3.1 when<br/>
            </c:when>
            <c:when test="${true}">
                3.2 when<br/>
            </c:when>
        </c:choose>
        
        <%--
            if(test1)
            else if(test2)
            else
        --%>
        <c:choose>
            <c:when test="${false}">
                4.1 when<br/>
            </c:when>
            <c:when test="${false}">
                4.2 when<br/>
            </c:when>
            <c:otherwise>
                4. otherwise <br/>
            </c:otherwise>
        </c:choose>
                
                
    <%--
        Fehler:
    
        <c:choose>
            <c:otherwise>
            </c:otherwise>
        </c:choose>
    
        <c:choose>
            <c:otherwise>
            </c:otherwise>
            <c:when test="${false}">
            </c:when>
        </c:choose>
    
        <c:choose>
            <c:when test="${false}">
            </c:when>
            <c:otherwise>
            </c:otherwise>
            <c:otherwise>
            </c:otherwise>
        </c:choose>
    --%>
        
                
                
    </body>
</html>
