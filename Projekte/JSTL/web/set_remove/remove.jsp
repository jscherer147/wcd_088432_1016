<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:set var="att" scope="page" value="String-Att"/>
        <c:set var="att" scope="request" value="${22}"/>
        <c:set var="att" scope="session" value="${-3.5}"/>
        <c:set var="att" scope="application" value="${true}"/>

        <h1>c:remove löscht in standardmäßig ALLEN scopes</h1>
        <c:remove var="att" scope="application" />
        
        att (page scope): ${pageScope.att} <br/>
        att (request scope): ${requestScope.att} <br/>
        att (session scope): ${sessionScope.att} <br/>
        att (appliction scope): ${applicationScope.att} <br/>
        
        <h1>c:set ohne scope kann auch in ALLEN scopes löschen</h1>
        <c:set var="att" value="${nichtda}" scope="session"/>
        
        att (page scope): ${pageScope.att} <br/>
        att (request scope): ${requestScope.att} <br/>
        att (session scope): ${sessionScope.att} <br/>
        att (appliction scope): ${applicationScope.att} <br/>
        
    </body>
</html>
