<%@page import="java.util.HashMap"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <h1>Neue einfache Attribute erzeugen</h1>
        
        <c:set var="x" value="Bin ein String" />
        
        x = ${x} <br/>
        Typ von x: ${x['class']} <br/>
        
        <c:set var="x" value="${12}" />
        Typ von x: ${x['class']} <br/>
        
        <c:set var="x" value="${12.0}" />
        Typ von x: ${x['class']} <br/>
        
        <c:set var="x" value="${true}" />
        Typ von x: ${x['class']} <br/>
        
        
        <h1>Alias-Namen bei komplexen Attribut-Namen / Zugriffen</h1>
        
        ${applicationScope['javax.servlet.context.tempdir']} <br/>
        <c:set var="alias" value="javax.servlet.context.tempdir"/>
        ${applicationScope[alias]} <br/><br/>
        
        id = ${pageContext.session.id} <br>
        <c:set var="alias" value="${pageContext.session.id}"/>
        id = ${alias} <br>
        
        
        <h1>Property einer JavaBean setzten</h1>
        
        <jsp:useBean id="d1" class="java.util.Date" scope="request"/>
        <jsp:setProperty name="d1" property="time" value="0"/>
        
        d1 = ${d1} <br/>
        
        <jsp:useBean id="d2" class="java.util.Date" scope="request"/>
        <c:set target="${requestScope.d2}" property="time" value="0"/>
        
        d2 = ${d2} <br/>

        <% pageContext.setAttribute("d3", new Date()); %>
        <c:set target="${d3}" property="time" value="0"/>
        d3 = ${d3} <br/>
        
        
        <h1>Key/Value in einer Map setzen</h1>
        <%
            pageContext.setAttribute("map", new HashMap<>());
        %>
        
        <%--
            setProperty arbeitet NUR mit Beans!!!
            <jsp:setProperty name="map" property="k1" value="v1"/>
        --%>
        
        <c:set target="${map}" property="k2" value="v2"/>
        map = ${map} <br/>
        
    </body>
</html>
