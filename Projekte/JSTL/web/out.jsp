<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <%
            //Gemeint: ein Client hat den Text unserer Anwendung übergeben
            //(z.B. Formular ausgefüllt und gesendet)
            
            //Die Anwendung hat die Daten (Texte) gespeichert
            //und präsentiert sie den anderen Clients.
            
            //Simuliertes Laden der davor gespeicherten User-Texte:
            String someClientText = "<script>alert('Hallo')</script>";
            pageContext.setAttribute("userText", someClientText);
        %>
        
        <c:out value="${userText}"/> <br/>
        <c:out value="${userText}" escapeXml="true"/> <br/>

        <c:out value="<b> steht für fett" escapeXml="true"/> <br/>
        <c:out value="<b> steht für fett </b>" escapeXml="false"/> <br/>
        
        <hr/>
        
        <c:out value="${userText}" default="Alternativer Text 1" /> <br/>
        <c:out value="${nichtda}" default="Alternativer Text 2" /> <br/>
        <c:out value="${nichtda}" >
            Alternativer Text 3. Line 1?<br/>
            Alternativer Text 3. Line 2?<br/>
        </c:out> <br/>
        
        <c:out value="${nichtda}" escapeXml="${not empty nichtda}" >
            Alternativer Text 4. Line 1?<br/>
            Alternativer Text 4. Line 2?<br/>
        </c:out> <br/>

        
    </body>
</html>
