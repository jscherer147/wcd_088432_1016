<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            int[] a1 = {
                7,
                17,
                -33,
            };
            pageContext.setAttribute("a1", a1);
        %>

        <c:forEach items="${a1}">
            wiederhole 
        </c:forEach>
        <br/>

        <c:forEach items="${a1}" var="element">
            ${element} 
        </c:forEach>
        <br/>
        
        
        <hr/>
        
        <%
            String[] a2 = {
                "mo", "di", "mi", "do", "fr", "sa", "so"
            };
            pageContext.setAttribute("a2", a2);
        %>
        
        <c:forEach items="${a2}" var="wochentag" varStatus="status" 
                   begin="1" end="5" step="2">
            ${status.count}. ${wochentag} <br/>
        </c:forEach>
        
        
        <hr/>
        
        <c:forEach items='${ pageScope["a2"] }' var="wochentag">
            ${wochentag} 
        </c:forEach>
        <br/>
            
        <c:forEach items="${ pageScope['a2'] }" var="wochentag">
            ${wochentag} 
        </c:forEach>
        <br/>
            
        <hr/>
        
    </body>
</html>
