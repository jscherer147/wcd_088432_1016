<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

    <c:forEach begin="1" end="3">
        wiederhole 
    </c:forEach>
    <br/>
        
    <c:forEach begin="1" end="3" var="i">
        ${i} 
    </c:forEach>
    <br/>
    Nach dem forEach-Body ist i weg. \${i} = ${i} 
    <br/>
    
    <%--    
    Exception begin < 0 
    (Nicht in der Prüfung)
    
    <c:forEach begin="-2" end="5" var="i" step="2">
        ${i} 
    </c:forEach>
    <br/>
    --%>
    
    <c:forEach begin="0" end="9" var="i" step="2">
        ${i} 
    </c:forEach>
    
    <hr/>
    
    <%
        for(int i=0, count=1; i<=9; i+=2, count++) {
            System.out.println(count + ". i = " + i);
        }
    %>
    
    <c:forEach begin="0" end="9" var="i" step="2" varStatus="status">
        ${status.count}. i = ${i} <br/>
    </c:forEach>
    <br/>
    
    </body>
</html>
