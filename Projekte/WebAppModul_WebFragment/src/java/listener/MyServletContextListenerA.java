package listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

public class MyServletContextListenerA implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("-- contextInitialized / A");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("-- contextDestroyed / A");
    }

}
