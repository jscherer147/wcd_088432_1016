package listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyServletContextListenerB implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("-- contextInitialized / B");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("-- contextDestroyed / B");
    }

}