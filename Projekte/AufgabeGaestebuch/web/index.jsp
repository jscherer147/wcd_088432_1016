<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <h1>Gästebuch</h1>

        <!-- 
            TODO: Zugriff auf listUserEntries synchronisieren
        -->

        <%--
        <jsp:useBean id="helper" class="java.lang.String" scope="application">
            <c:forEach items="${listUserEntries}" var="userEntry">
                Datum: ${userEntry.date} <br/>
                Name: ${userEntry.userName} <br/>
                Eintrag: ${userEntry.text} <br/>
                <br/>
            </c:forEach>
            <c:remove var="helper"/>
        </jsp:useBean>
        --%>

        <c:forEach items="${listUserEntries}" var="userEntry">
            Datum: ${userEntry.date} <br/>
            Name: ${userEntry.userName} <br/>
            Eintrag: <c:out value="${userEntry.text}"/> <br/>
            <br/>
        </c:forEach>


        <hr/>

        <form action="entry" method="post">
            Name: <input name="userName"/> <br/>

            Text: <br/>
            <textarea rows="5" cols="50" name="text">Hier den Text eingeben</textarea>

            <br/>
            <input type="submit"/>
        </form>

    </body>
</html>
