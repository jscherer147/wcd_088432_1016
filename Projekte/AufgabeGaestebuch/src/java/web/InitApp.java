package web;

import java.util.LinkedList;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

//@WebListener
public class InitApp implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("listUserEntries", new LinkedList());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

}
