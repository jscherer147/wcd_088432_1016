package web;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import user.UserEntry;

//@WebServlet("/entry")
public class ServletNewEntry extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//TODO: Request-Parameter validieren

        String userName = req.getParameter("userName");
        String userText = req.getParameter("text");
     
        UserEntry entry = new UserEntry(userName, userText);
        
        ServletContext servletContext = getServletContext();
        
        synchronized( servletContext ) {
            List<UserEntry> listEntries 
                    = (List)servletContext.getAttribute("listUserEntries");

            listEntries.add(entry);
        }
        
//        resp.sendRedirect("index.jsp");
        resp.sendRedirect( servletContext.getContextPath() );
    }
    
}
