package user;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UserEntry {
    
    private final LocalDateTime date;
    private String userName;
    private String text;

    public UserEntry(String userName, String text) {
        this.userName = userName;
        this.text = text;
        
        date = LocalDateTime.now();
    }

    public String getDate() {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd.MM.uu HH:mm");
        
        return fmt.format(date);
    }

    public String getUserName() {
        return userName;
    }

    public String getText() {
        return text;
    }
    
    
    
}
