package filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Locale;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class FilterUppercase implements Filter {
    
//    private static class MyResponse implements HttpServletResponse {
//        
//        private HttpServletResponse realResponse;
//
//        @Override
//        public PrintWriter getWriter() throws IOException {
//            return new PrintWriter(System.out);
//        }
//
//        @Override
//        public void addCookie(Cookie cookie) {
//            realResponse.addCookie(cookie);
//        }
//
//        @Override
//        public boolean containsHeader(String name) {
//            return realResponse.containsHeader(name);
//        }
//        
//        //...
//    }
        
    private static class MyResponse extends HttpServletResponseWrapper {

        private PrintWriter out;
        private StringWriter sw;
        
        public MyResponse(HttpServletResponse response) {
            super(response);
            
            sw = new StringWriter();
            out = new PrintWriter(sw);
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            return out;
        }
        
        String getText() {
            out.flush();
            return sw.toString();
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        
        HttpServletResponse realResponse = (HttpServletResponse)response;
        MyResponse myResponse = new MyResponse(realResponse);
        
        chain.doFilter(request, myResponse);
        
        String text = myResponse.getText();
        text = text.toUpperCase();
        
        response.getWriter().print(text);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }

}
