package filter;

import java.io.IOException;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/*

    <filter>
        <filter-name>Simple</filter-name>
        <filter-class>filter.SimpleFilter</filter-class>
        
        <async-supported>false</async-supported>
        <init-param>
            <param-name>firmenname</param-name>
            <param-value>MyCompany</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>Simple</filter-name>
        <url-pattern>/testSimpleFilter</url-pattern>
        <url-pattern>/url2</url-pattern>
        <servlet-name>ServletTestSimpleFilter</servlet-name>
        <servlet-name>ServletWoche</servlet-name>

        <dispatcher>REQUEST</dispatcher>
        <dispatcher>FORWARD</dispatcher>
    </filter-mapping>
*/

@WebFilter(
    filterName = "Simple",
        
//    value = { "/testSimpleFilter", "/url2" },
    urlPatterns = { "/testSimpleFilter", "/url2" },
    servletNames = { "ServletWoche", "ServletTestSimpleFilter" },
    asyncSupported = false,
    initParams = { @WebInitParam(name = "firmenname", value = "MyCompany") },
    
    dispatcherTypes = { DispatcherType.REQUEST, DispatcherType.FORWARD }
)
public class SimpleFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        
        chain.doFilter(request, response);
        
    }

    @Override
    public void destroy() {
    }

}
