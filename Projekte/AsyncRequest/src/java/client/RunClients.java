package client;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Ein sehr grober Test einer Belastung
 *
 */

/*
+++++ Test synchron ++++++
CLIENTS: 3
Gesamtzeit: 2.066 Sek.
Gesamtzeit: 2066 mSek.
+++++ Test ASYNCHRON ++++++
CLIENTS: 3
Gesamtzeit: 2.109 Sek.
Gesamtzeit: 2109 mSek.

+++++ Test synchron ++++++
CLIENTS: 20
Gesamtzeit: 2.178 Sek.
Gesamtzeit: 2178 mSek.
+++++ Test ASYNCHRON ++++++
CLIENTS: 20
Gesamtzeit: 2.164 Sek.
Gesamtzeit: 2164 mSek.

+++++ Test synchron ++++++
CLIENTS: 100
Gesamtzeit: 2.886 Sek.
Gesamtzeit: 2886 mSek.
+++++ Test ASYNCHRON ++++++
CLIENTS: 100
Gesamtzeit: 3.073 Sek.
Gesamtzeit: 3073 mSek.

+++++ Test synchron ++++++
CLIENTS: 200
Gesamtzeit: 2.944 Sek.
Gesamtzeit: 2944 mSek.
+++++ Test ASYNCHRON ++++++
CLIENTS: 200
Gesamtzeit: 3.544 Sek.
Gesamtzeit: 3544 mSek.

+++++ Test synchron ++++++
CLIENTS: 300
Gesamtzeit: 5.992 Sek.
Gesamtzeit: 5992 mSek.
+++++ Test ASYNCHRON ++++++
CLIENTS: 300
Gesamtzeit: 5.155 Sek.
Gesamtzeit: 5155 mSek.

+++++ Test synchron ++++++
CLIENTS: 500
Gesamtzeit: 8.314 Sek.
Gesamtzeit: 8314 mSek.
+++++ Test ASYNCHRON ++++++
CLIENTS: 500
Gesamtzeit: 6.337 Sek.
Gesamtzeit: 6337 mSek.


+++++ Test synchron ++++++
CLIENTS: 600
Gesamtzeit: 11.394 Sek.
Gesamtzeit: 11394 mSek.
+++++ Test ASYNCHRON ++++++
CLIENTS: 600
Gesamtzeit: 7.462 Sek.
Gesamtzeit: 7462 mSek.

+++++ Test synchron ++++++
CLIENTS: 1000
Gesamtzeit: 18.522 Sek.
Gesamtzeit: 18522 mSek.
+++++ Test ASYNCHRON ++++++
CLIENTS: 1000
Gesamtzeit: 10.976 Sek.
Gesamtzeit: 10976 mSek.

+++++ Test synchron ++++++
CLIENTS: 1600
Gesamtzeit: 32.345 Sek.
Gesamtzeit: 32345 mSek.
+++++ Test ASYNCHRON ++++++
CLIENTS: 1600
Gesamtzeit: 15.506 Sek.
Gesamtzeit: 15506 mSek.

+++++ Test synchron ++++++
CLIENTS: 2000
Gesamtzeit: 43.816 Sek.
Gesamtzeit: 43816 mSek.
+++++ Test ASYNCHRON ++++++
CLIENTS: 2000
Gesamtzeit: 18.65 Sek.
Gesamtzeit: 18650 mSek.

*/
public class RunClients {
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    static final String adresse = "http://localhost:8084/AsyncRequest/sync";
    static final String adresse = "http://localhost:8084/AsyncRequest/async";
    static final int ANZAHL_CLIENTS = 2000;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    static class ClientRequestThread extends Thread {

        @Override
        public void run() {
            try {

                URL url = new URL(adresse);
                try (BufferedReader in
                        = new BufferedReader(new InputStreamReader(url.openStream()))) {
                    String line;
                    while ((line = in.readLine()) != null) {
//                        System.out.println(line);
                    }
                }
            } catch (Exception e) {
                System.out.println("### Client-Fehler: " + e.getMessage());
            }
        }

        public static void main(String[] args) throws InterruptedException {

            List<Thread> clientThreads = new ArrayList<>();

            for (int i = 0; i < ANZAHL_CLIENTS; i++) {
                clientThreads.add(new ClientRequestThread());
            }

            long start = System.currentTimeMillis();

            for (Thread th : clientThreads) {
                th.start();
            }

            for (Thread th : clientThreads) {
                th.join();
            }

            long ende = System.currentTimeMillis();

            if (adresse.endsWith("/sync")) {
                System.out.println("+++++ Test synchron ++++++");
            } else {
                System.out.println("+++++ Test ASYNCHRON ++++++");
            }
            System.out.println("CLIENTS: " + ANZAHL_CLIENTS);
            System.out.println("Gesamtzeit: " + (ende - start) / 1000. + " Sek.");
            System.out.println("Gesamtzeit: " + (ende - start) + " mSek.");
        }

    }
}
