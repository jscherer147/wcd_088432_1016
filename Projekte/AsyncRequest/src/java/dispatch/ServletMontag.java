/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dispatch;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "ServletMontag", 
    urlPatterns = {"/montag"},
    asyncSupported = true
)
public class ServletMontag extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        AsyncContext asyncContext = request.startAsync();
        asyncContext.addListener(new MyAsyncListener());
                
        new Thread() {
            @Override
            public void run() {
                processRequest(request, response);
                asyncContext.complete();
            }
        }.start();
        
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletMontag</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServletMontag at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } catch(IOException e) {
            e.printStackTrace();
        }
    }



}
