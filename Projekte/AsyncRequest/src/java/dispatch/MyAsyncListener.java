package dispatch;

import java.io.IOException;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;

public class MyAsyncListener implements AsyncListener {

    @Override
    public void onStartAsync(AsyncEvent event) throws IOException {
        System.out.println("--> onStartAsync. this: " + this);
    }
    
    @Override
    public void onComplete(AsyncEvent event) throws IOException {
        System.out.println("--> onComplete. this: " + this);
    }

    @Override
    public void onTimeout(AsyncEvent event) throws IOException {
    }

    @Override
    public void onError(AsyncEvent event) throws IOException {
    }


}
