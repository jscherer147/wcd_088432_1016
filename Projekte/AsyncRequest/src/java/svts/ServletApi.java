/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package svts;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "ServletApi", 
    urlPatterns = {"/api"},
    asyncSupported = true       //Exam!
)
public class ServletApi extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        AsyncContext asyncContext = req.startAsync(); //???

        asyncContext.addListener(new AsyncListener() {
            //Exam:
            public void onComplete(AsyncEvent event) throws IOException {
            }

            public void onTimeout(AsyncEvent event) throws IOException {
            }

            public void onError(AsyncEvent event) throws IOException {
            }

            //Exam:
            public void onStartAsync(AsyncEvent event) throws IOException {
            }
        });
        
/*        
        //unwahrscheinlich in der Exam: start
        asyncContext.start(new Runnable() {
            @Override
            public void run() {
                //unwahrscheinlich in der Exam: getRequest, getResponse
                HttpServletRequest request = (HttpServletRequest)asyncContext.getRequest();
                HttpServletResponse response = (HttpServletResponse)asyncContext.getResponse();
                
                processRequest(request, response);
                
                asyncContext.complete(); //???
            }
        });
*/        
        
        //Exam!
//        asyncContext.dispatch();
//        asyncContext.dispatch("/target");
//        asyncContext.dispatch(foreignContext, "/target");
        
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletApi</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServletApi at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

}
