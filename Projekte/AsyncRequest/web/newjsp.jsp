<%-- 
    Document   : newjsp
    Created on : 10.10.2016, 11:15:39
    Author     : apatrin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        
        ${ pageContext.servletConfig.getInitParameter("param1") }
        
    </body>
</html>
