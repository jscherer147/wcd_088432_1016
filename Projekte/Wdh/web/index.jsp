<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            Map<String, Object> map = new TreeMap<>();
            ArrayList<Date> list = new ArrayList<>();
            map.put("var", list);
            
            list.add(new Date());
            list.add(new Date(100000L));
            list.add(new Date(232323232323L));
            
            application.setAttribute("mymap", map);
        %>
        
        <c:forEach items="${mymap.var}" var="element">
            ${element.time} <br/>
        </c:forEach>
        
    </body>
</html>
