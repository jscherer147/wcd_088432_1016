package beans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MyDateFormatter {
    
    private Locale locale = Locale.getDefault();
    private int dateStyle = DateFormat.FULL;
    private int timeStyle = DateFormat.FULL;
    private Date date = new Date();
    
    public void setLanguage(String lang) {
        
        switch(lang) {
            case "de":
            case "en":
            case "fr":
                locale = new Locale(lang);
                break;
            default:
                throw new IllegalArgumentException("Language not supported: " + lang);
        }
    }
    public void setDate(String date) throws ParseException {
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        
        this.date = df.parse(date);
    }
    
    public String getFormattedDate() {
        
        DateFormat df = DateFormat.getDateTimeInstance(dateStyle, timeStyle, locale);
        
        return df.format(date);
    }
    
    public void setDateStyle(String dateStyle) {
        this.dateStyle = parseStyle(dateStyle);
    }
    public void setTimeStyle(String timeStyle) {
        this.timeStyle = parseStyle(timeStyle);
    }

    private int parseStyle(String style) {
        switch( style ) {
            case "SHORT":
                return DateFormat.SHORT;
            case "MEDIUM":
                return DateFormat.MEDIUM;
            case "LONG":
                return DateFormat.LONG;
            case "FULL":
                return DateFormat.FULL;
            default:
                throw new IllegalArgumentException("Style not supported: " + style);
        }
    }
}
