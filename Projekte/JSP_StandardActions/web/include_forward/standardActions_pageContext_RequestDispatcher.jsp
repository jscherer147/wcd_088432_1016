<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <%
//            RequestDispatcher rd = request.getRequestDispatcher("/target.jsp");
//            rd.forward(request, response);

//              pageContext.forward("target.jsp");
        %>
        <%--
            Scriptfrei weiterleiten:
        
        <jsp:forward page="target.jsp"/>
        --%>

        <%
//            RequestDispatcher rd = request.getRequestDispatcher("/target.jsp");
//            rd.include(request, response);

//            pageContext.include("target.jsp");
        %>
        <%--
            Scriptfrei 'einbinden':
        
        <jsp:include page="target.jsp"/>
        --%>
        
        
    </body>
</html>
