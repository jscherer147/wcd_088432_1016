<%-- 
    Document   : normal
    Created on : 31.08.2016, 11:09:33
    Author     : apatrin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%@include file="text.txt" %>

        <jsp:scriptlet>
            int x = 3;
        </jsp:scriptlet>
        
        <%
            int x2 = 4;
        %>
    </body>
</html>

