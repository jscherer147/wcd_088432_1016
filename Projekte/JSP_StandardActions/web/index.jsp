<%-- 
    Controller bereitet Attribue vor:
--%>
<%  
    request.setAttribute("date", new java.util.Date());
    
    // danach weiterleiten zu der View (JSP)
    //...
%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        Eine Jsp findet das Attribut und setzt für die Ausgabe ein: <br/>
        Time: ${date.time} <br/>
        
        
    </body>
</html>
