<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <h1>Datum formatieren</h1>
        
        <form action="formatDate.jsp" method="post" >
            
            <h3>Datum:</h3>
            <input name="date" value="1.1.2014"/> Muster: (TT.MM.JJJJ)
            
            <h3>Sprache:</h3>
            <input type="radio" name="lang" value="de" checked /> deutsch <br/>
            <input type="radio" name="lang" value="en"/> englisch <br/>
            <input type="radio" name="lang" value="fr"/> französisch <br/>
            
            <h3>Datum-Stil</h3>
            <input type="radio" name="dateStyle" value="SHORT"/> SHORT <br/>
            <input type="radio" name="dateStyle" value="MEDIUM" checked/> MEDIUM <br/>
            <input type="radio" name="dateStyle" value="LONG"/> LONG <br/>
            <input type="radio" name="dateStyle" value="FULL"/> FULL <br/>
                
            <h3>Zeit-Stil</h3>
            <input type="radio" name="timeStyle" value="SHORT"/> SHORT <br/>
            <input type="radio" name="timeStyle" value="MEDIUM" checked/> MEDIUM <br/>
            <input type="radio" name="timeStyle" value="LONG"/> LONG <br/>
            <input type="radio" name="timeStyle" value="FULL"/> FULL <br/>
                
                
            <hr/>
            <input type="submit"/>
        </form>
        
        
    </body>
</html>
