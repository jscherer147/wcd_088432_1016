<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Datum formatiert:</h1>
        
        <jsp:useBean id="formatter" class="beans.MyDateFormatter" />
        
        <jsp:setProperty name="formatter" property="*"/>
        <jsp:setProperty name="formatter" property="language" param="lang"/>
        
        <h2><jsp:getProperty name="formatter" property="formattedDate"/></h2>
        
        <h2>${formatter.formattedDate}</h2>
    </body>
</html>
