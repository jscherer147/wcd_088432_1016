<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <jsp:useBean id="d1" class="java.util.Date" scope="request">
            <jsp:setProperty name="d1" property="time" value="0"/>
        </jsp:useBean>
        d1 = ${d1} <br/>
        
        <jsp:useBean id="d2" scope="request">
            <jsp:attribute name="class">java.util.Date</jsp:attribute>
            <jsp:body>
                <jsp:setProperty name="d1" property="time" value="0"/>
            </jsp:body>
        </jsp:useBean>
    </body>
</html>
