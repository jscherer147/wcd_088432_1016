<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <jsp:include page="menu.jsp?title=Menu 1"/> 
        <hr/>
        
        <jsp:include page="menu.jsp">
            <jsp:param name="title" value="Menu 2"/>
        </jsp:include>
        
    </body>
</html>
