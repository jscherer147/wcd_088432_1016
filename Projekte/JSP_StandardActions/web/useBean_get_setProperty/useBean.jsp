<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <jsp:useBean id="d1" class="java.util.Date" >
            Hallo aus dem Body der useBean<br/>
            ${1 + 1}
        </jsp:useBean>
        
        <%-- 
            1. Suche nach dem Attribut 'd1' (id) im Page-Scope (kein scope).

               Dafür eine lokale Variable 'd1' (id) vom Typ 'java.util.Date' (class)
                    anlegen:

                java.util.Date d1 = null;        
                d1 = (java.util.Date) pageContext.getAttribute("d1", PageContext.PAGE_SCOPE);
        
            2. Falls das Attribut nicht gefunden wurde: neues Attribut erstellen.
        
               Dafür ein neues Objekt vom Typ 'java.util.Date' (class) erzeugen
               und unter dem Namen 'd1' (id) im Page-Scope (kein class) speichern:
        
                if(d1 == null) {
                    d1 = new java.util.Date();
        
                    pageContext.setAtrtibute("d1", d1, PageContext.PAGE_SCOPE);
        
                    //Falls es den useBean-Body gibt: der wird ausgewertet
                }
        --%>
        
        d1 = <%= d1 %> <br/>
        d1 = ${d1} <br/>
        
        <hr/>
        
        <%
            //DateFormat df = DateFormat.getDateInstance(); //Abstrakte Klasse
            //DateFormat df2 = new SimpleDateFormat();      //Konkrete Klasse
            
        %>
        
        <jsp:useBean id="formatter" type="java.text.DateFormat" 
                     class="java.text.SimpleDateFormat" scope="request"/>
        
        <%--
        
            1. Nach dem Attribut 'formatter' (id) im Request-Scope (scope) suchen.
                Dafür eine lokale Variable 'formatter' (id)
                vom Typ 'java.text.DateFormat' (type) anlegen.
                
            java.text.DateFormat formatter = null;
            formatter = (java.text.DateFormat) pageContext.getAttribute("formatter", 
                            PageContext.REQUEST_SCOPE);
        
            2. Falls das Attribut nicht gefunden wird,
                ein neues Attribut vom Typ 'java.text.SimpleDateFormat' erzeugen:
        
            if(formatter==null) {
                formatter = new java.text.SimpleDateFormat();
                pageContext.setAttribute("formatter", formatter);
        
                //Falls es den useBean-Body gibt: der wird ausgewertet
            }
        --%>
        
        formatter: ${formatter} <br/>
        formatter: <%= formatter %> <br/>
        
        
        <hr/>
        
        <%
             request.setAttribute("d2", new java.util.Date()); //im Controller
        %>
        <jsp:useBean id="d2" type="java.util.Date" scope="request" />
        
        ${d2.time}
        
        <%--
            1. Suche nach dem Attribut 'd2' (id) im Request-Scope (kein scope).

               Dafür eine lokale Variable 'd2' (id) vom Typ 'java.util.Date' (class)
                    anlegen:

                java.util.Date d2 = null;        
                d2 = (java.util.Date) pageContext.getAttribute("d2", PageContext.REQUEST_SCOPE);
        
            2. Falls das Attribut nicht gefunden wurde: Exception werfen
        
                if(d2 == null) {
                    throw new InstantiationException(...);
        
                    //Falls es den useBean-Body gibt, wird er IGNORIERT
                }
        
        --%>
        
    </body>
</html>
