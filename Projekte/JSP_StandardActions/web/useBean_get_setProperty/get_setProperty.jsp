<%@page import="beans.Person"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <jsp:useBean id="d1" class="java.util.Date"/>
        d1.time = ${d1.time} <br/>
        d1.time = <jsp:getProperty name="d1" property="time"/> <br/>
        
        <%
            pageContext.setAttribute("d2", new Date());
        %>
        d2.time = ${d2.time} <br/>
        <%--
            Exception. Bean d2 wurde nicht mit der useBean registriert:
        
            d2.time = <jsp:getProperty name="d2" property="time"/> <br/>
        --%>
        
        
        <hr/>
        
        <jsp:useBean id="d3" class="java.util.Date">
            <jsp:setProperty name="d3" value="0" property="time"/>
        </jsp:useBean>
        
        d3 = ${d3} <br/>
        
        
        <hr/>
        <% 
            pageContext.setAttribute("d4", new Date(), PageContext.REQUEST_SCOPE);
        %>
        
        <jsp:useBean id="d4" class="java.util.Date" scope="request">
            <jsp:setProperty name="d4" property="time" value="0"/>
        </jsp:useBean>
        
        d4 = ${d4} <br/>
        d4 aus dem Request-Scope: ${requestScope.d4} <br/>
        
        <hr/>
        
        <jsp:setProperty name="d4" property="time" value="0"/> 
        d4.time = ${d4.time} <br/>
        
        <jsp:setProperty name="d4" property="time" value="<%= 22 - 2 %>"/> 
        d4.time = ${d4.time} <br/>
        
        <jsp:setProperty name="d4" property="time" value="${33.1*3}"/> 
        d4.time = ${d4.time} <br/>
        
        <hr/>
        <jsp:setProperty name="d4" property="time" param="zeit"/> 
        d4.time = ${d4.time} <br/>
        
        <a href="get_setProperty.jsp?zeit=103">sende Request-Parameter zeit=103</a>
        
        <hr/>
        <%
            pageContext.setAttribute("p1", new Person("M.", "Mustermann"));
        %>
        <jsp:useBean id="p1" type="beans.Person"/>
        <jsp:setProperty name="p1" property="vorname" value="Max"/>
        p1 = ${p1} <br/>
        
        <hr/>
        <h3>property="*"</h3>
                
        <jsp:useBean id="p2" class="beans.Person" scope="request">
            <jsp:setProperty name="p2" property="*"/>
        </jsp:useBean>
        
        p2 = ${p2} <br/>
        
        <a href="get_setProperty.jsp?vorname=Otto&nachname=Schulz">sende 
            Request-Parameter vorname und nachname</a> <br/>
    </body>
</html>
