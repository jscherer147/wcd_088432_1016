package listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;

public class MyServletContextAttributeListener implements ServletContextAttributeListener {

    @Override
    public void attributeAdded(ServletContextAttributeEvent event) {
        printInfos(event, "attributeAdded");
    }

    @Override
    public void attributeRemoved(ServletContextAttributeEvent event) {
        printInfos(event, "attributeRemoved");
    }

    @Override
    public void attributeReplaced(ServletContextAttributeEvent event) {
        printInfos(event, "attributeReplaced");
    }

    private void printInfos(ServletContextAttributeEvent event, String method) {
        System.out.println("******* " + method + " ********");
        
        String name = event.getName();
        System.out.println("name: " + name);
        
        Object value = event.getValue();
        System.out.println("value from event: " + value);
        
        ServletContext context = event.getServletContext();
        Object valueActual = context.getAttribute(name);
        System.out.println("value from appliaction scope: " + valueActual);
    }
    
}
