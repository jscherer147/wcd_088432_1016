package listener;

import java.io.Serializable;
import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionEvent;

public class MyHttpSessionActivationListener 
        implements HttpSessionActivationListener, Serializable {

    @Override
    public void sessionWillPassivate(HttpSessionEvent se) {
        System.out.println("----> MyHttpSessionActivationListener / sessionWillPassivate");
    }

    @Override
    public void sessionDidActivate(HttpSessionEvent se) {
        System.out.println("----> MyHttpSessionActivationListener / sessionDidActivate");
    }

}
