package utils;

import java.time.LocalDate;
import java.util.Date;
import javax.servlet.jsp.PageContext;

public class DateUtils {

    public static Date getDatum() {
        return new Date(); //LocalDate.now()
    }

    public static java.util.Date tageDazuRechnen(java.util.Date d, int tage) {

        long time = d.getTime();
        time += 1000L * 60 * 60 * 24 * tage;
        return new Date(time);
    }

    public static void erstelleDatum(String name, String scopeName,
            PageContext pageContext) {
//TODO: NullPointer (verursacht durch null-Argumente) deutlicher gestalten
        int scope;

        switch (scopeName) {
            case "page":
                scope = PageContext.PAGE_SCOPE;
                break;
            case "request":
                scope = PageContext.REQUEST_SCOPE;
                break;
            case "session":
                scope = PageContext.SESSION_SCOPE;
                break;
            case "application":
                scope = PageContext.APPLICATION_SCOPE;
                break;
            default:
                throw new IllegalArgumentException("Unknown scope: " + scopeName);
        }

        pageContext.setAttribute(name, new Date(), scope);
    }

}
