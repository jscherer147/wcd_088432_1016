<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Klammer mit einer Bean</h1>
        
        <jsp:useBean id="d1" class="java.util.Date"/>
        
        \${d1.time} = ${d1.time} <br/>
        Falsch: \${d1[time]} = ${d1[time]} <br/>
        \${d1['time']} = ${d1['time']} <br/>
        \${d1["time"]} = ${d1["time"]} <br/>
        
        <%
            application.setAttribute("prop", "time");
        %>
        
        \${d1[prop]} = ${d1[prop]} <br/>
        
        
        <h1>Klammer mit einer Map</h1>
        
        <%
            Map<String, Object> map = new TreeMap<>();
            map.put("one", "Einz");
            map.put("one-zero", 10);
            map.put("two-zero", 20);
            
            pageContext.setAttribute("map", map);
        %>
        
        \${map.one} = ${map.one} <br/>
        Falsch: \${map.two-zero} = ${map.two-zero} <br/>
        
        \${map['one']} = ${map['one']} <br/>
        \${map['two-zero']} = ${map['two-zero']} <br/>
        
        
        
        <h3>Nicht in der Prüfung: eine Map mit nicht-String-Schlüßel</h3>
        
        <%
            Map<Long, Object> m1 = new TreeMap();
            m1.put(1L, "einz");
            m1.put(2L, "zwei");
            
            pageContext.setAttribute("map2", m1);
            
            pageContext.setAttribute("key1", 1L);
            pageContext.setAttribute("key2", 2);
        %>
        
        \${map2[1]} = ${map2[1]} <br/>
        \${map2[key1]} = ${map2[key1]} <br/>
        
        <%--
        ClassCastException: java.lang.Long cannot be cast to java.lang.Integer
        \${map2[key2]} = ${map2[key2]} <br/>
        --%>
        
        <%-- Weiter mit den Prüfungsinhalten:--%>
        
        <h1>Klammer mit Arrays:</h1>
        
        <%
            int[] arr1 = { 100, 200, 300 };
            pageContext.setAttribute("a1", arr1);
        %>
        
        \${a1[1]} = ${a1[1]} <br/>
        Nichts: \${a1[100]} = ${a1[100]} <br/>
        Nichts: \${a1[-1]} = ${a1[-1]} <br/>
        
        
        <%  pageContext.setAttribute("index", 2); %>
        \${a1[index]} = ${a1[index]} <br/>
        
        <%--
        <%  pageContext.setAttribute("index", "Hallo"); %>
        
        NumberFormatException:
        \${a1[index]} = ${a1[index]} <br/>
        --%>
        
        
        <h1>Klammer mit Listen</h1>
        <%
            List<String> list = Arrays.asList("mo", "di", "mi");
            pageContext.setAttribute("list", list);
        %>
        
        \${list[0]} = ${list[0]} <br/>
        Nichts: \${list[100]} = ${list[100]} <br/>
        
    </body>
</html>
