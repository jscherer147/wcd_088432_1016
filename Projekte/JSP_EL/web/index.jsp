<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Die primäre Aufgabe vom EL: suche nach einem Attribut 
            mit einem EL-Identifier</h1>
        <%-- EL: --%>
        ${att}
        
        <%-- Java: --%>
        <%
            Object erg = pageContext.findAttribute("att");
            if(erg==null) {
                erg = "";
            }
            out.print(erg);
        %>

        
        
        <h2>EL-Auswertung ausschalten</h2>
        
        Keine Auswertung (Escaping): \${att} <br/>
        
        Im DD. <br/>
        <%--
        <jsp-config>
            <jsp-property-group>
                <url-pattern>*.jsp</url-pattern>
                <el-ignored>true</el-ignored>
            </jsp-property-group>
        </jsp-config>
        --%>
        
        Mit der page-Directive, isELIgnored=true<br/>


        
        
        <h2>EL-Identifier</h2>
        
        Gültig: Wie in Java (Buchstaben, Unterstrich...) <br/>
        Ungültig: Java Keywords (class, int...) <br/>
        Ungültig: EL Keywords (div, empty...) <br/>
        Ungültig: EL implizite Objekte (pageScope, pageContext...) <br/>
        
        
        <h2>EL-Literale</h2>
        
        Ganze Zahl: \${22} = ${22} <br/>
        Gleikomma-Zahl: \${22.3} = ${22.3} <br/>
        String: \${'hallo'} = ${'hallo'} <br/>
        String: \${"hallo"} = ${"hallo"} <br/>
        Boolean: \${true} = ${true} <br/>
        
        <h2>Java in EL. In der Prüfung immer falsch</h2>
        Bitte in der Prüfung nicht akzeptieren: ${ pageContext.getAttribute("att") }
        
    </body>
</html>
