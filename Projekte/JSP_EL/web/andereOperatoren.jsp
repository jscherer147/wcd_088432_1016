<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        HeadFirst-Buch, Pdf-Seite 430, Buchseite 396
        
        <h1>Aritmetische</h1>
        
        <h2> / heißt auch div </h2>
        <h2> % heißt auch mod </h2>
        
        \${1 + 1} = ${1 + 1} <br/>
        \${'2' + 2} = ${'2' + 2} <br/>
        \${"2" + 2} = ${"2" + 2} <br/>
        
        <%
            pageContext.setAttribute("x", 12);
            pageContext.setAttribute("y", 12f);
            pageContext.setAttribute("z", "12");
        %>
        
        \${x + 2} = ${x + 2} <br/>
        \${y + 2} = ${y + 2} <br/>
        \${z + 2} = ${z + 2} <br/>
        
        <%
//            out.print(5 / 0); //ArithmeticException
        %>
        Java. 5 / 0.0 = <%= 5/0. %> <br/>
        \${5 / 0} = ${5 / 0} <br/>
        
        Java. 5 / 2 = <%= 5/2 %> <br/>
        \${5 / 2} = ${5 / 2} <br/>
        
        <%--
            NumberFormatException:
        
            ${ x + "hallo" } <br/>
        --%>
            
        \${ 12 + null } = ${ 12 + null } <br/>
        \${ 12 + nichtda } = ${ 12 + nichtda } <br/>

        
        \${ 5 % 2 } = ${ 5 % 2 } <br/>
        \${ 5 mod 2 } = ${ 5 mod 2 } <br/>
        
        <%--
        ArithmeticException:
        
        \${ 5 % 0 } = ${ 5 % 0 } <br/>
        --%>
           
        
        <h1>Vergleichsoperatoren:</h1>
        <h2>Alias bitte im Buch nachschlagen: le, gt, eq, ne...  </h2>
        
        <%
            Integer i1 = new Integer(20);
            Integer i2 = new Integer(20);
        %>
        
        Java (Referenzenvergleich). 
            i1 == i2: <%= i1 == i2 %> <br/>
        
        <%
            pageContext.setAttribute("i1", i1);
            pageContext.setAttribute("i2", i2);
        %>
        
        Vergleich der Inhalte mit equals.
            \${ i1 == i2 }: ${ i1 == i2 } <br> <br/>
            
        
        \${ 12 > 10 } = ${ 12 > 10 } <br/>
        \${ "12" > 10 } = ${ "12" > 10 } <br/>
        \${ x > 10 } = ${ x > 10 } <br/>
        \${ nichtda > 10 } = ${ nichtda > 10 } <br/>
        \${ null > 10 } = ${ null > 10 } <br/>
        
        <%--
        Exception:
        \${ "zwanzig" > 10 } = ${ "zwanzig" > 10 } <br/>
        --%>
        
        
        <h1>Logische Operatoren</h1>
        <h2>Alias-Namen bitte lernen: not, and, or, empty</h2>
        
        Boolean.valueOf("tRuE"): <%= Boolean.valueOf("tRuE") %> <br/>
        
        \${ 'tRue' && "TrUe" } = ${ 'tRue' && "TrUe" } <br>
        
        \${not true} = ${not true} <br>
        \${!true} = ${!true} <br>
        
        \${!null} = ${!null} <br>
        \${not nichtda} = ${not nichtda} <br>
        \${not "hallo"} = ${not "hallo"} <br>
        
        <%--
        Cannot convert 11 of type class java.lang.Long to class java.lang.Boolean
        \${not 11} = ${not 11} <br>
        --%>
        
        
        <h2>Extra Bsp. zu empty</h2>
        
        Kein Attribut:
        \${empty att} = ${empty att} <br>
        
        <% pageContext.setAttribute("att", -33); %>
        Attribut gefunden:
        \${empty att} = ${empty att} <br>
        
        <% pageContext.setAttribute("att", new ArrayList<>()); %>
        Attribut gefunden, hat aber isEmpty(), die true liefert:
        \${empty att} = ${empty att} <br>
        
        <% pageContext.setAttribute("att", Arrays.asList(1, 2, 3) ); %>
        Attribut gefunden, hat isEmpty(), die false liefert:
        \${empty att} = ${empty att} <br>
        
        
        <h1>Nicht ein der Prüfung:</h1>
        test: ${ x == 12 ? "Meine Zahl ist 12" : "Nicht die 12" } <br/>
        
        
        <h1>Prioritäten</h1>
        \${ x == 12 && y == 12 } = ${ x == 12 && y == 12 }<br/>
        
    </body>
</html>
