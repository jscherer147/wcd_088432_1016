<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Punkt mit einer Bean</h1>
        
        <jsp:useBean id="d1" class="java.util.Date" scope="request"/>
        \${d1} = ${d1} <br/>
        \${d1.time} = ${d1.time} <br/>

        \${nichtda.time} = ${nichtda.time} <br/>

        <%-- PropertyNotFoundException:
        \${d1.nichtda} = ${d1.nichtda} <br/>
        --%>
        
        
        <h1>Punkt mit einer Map</h1>
        <%
            Map<Object, Object> m1 = new HashMap<>();
            m1.put(22, "zweiundzwanzig");
            m1.put("mo", 1);
            m1.put("di", "Dienstag");
            m1.put("do", new Date());
            m1.put("bester Tag", "Freitag");
            
            session.setAttribute("m1", m1);
        %>
        
        \${m1.di} = ${m1.di} <br/>
        <%--
        ELException ( do ist eine Java-Schlüßelwort):
        
        \${m1.do} = ${m1.do} <br/>
        --%>

        <%--
        Unerlaubter Schlüßelname für den Punkt-Operator:
        
        ${m1.bester Tag}
        ${m1.22}
        --%>
        
        
        
    </body>
</html>
