<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <%
            Map<String, String> map = new HashMap<>();
            map.put("a", "c");
            map.put("c", "Donnerstag");
        
            pageContext.setAttribute("map", map);
        %>
        
        \${map.c} = ${map.c} <br/>
        Falsch: \${map[c]} = ${map[c]} <br/>
        \${map["c"]} = ${map["c"]} <br/>
        \${map['c']} = ${map['c']} <br/>
        \${ map[ map['a'] ] } = ${ map[ map['a'] ] } <br/>
        
        
        <hr/>
        <%
            //Nur solange wir keine impliziten EL-Objekte kennen, das Attribut s1:
            pageContext.setAttribute("s1", session);
            //Nur solange wir keine impliziten EL-Objekte kennen, das Attribut pc:
            pageContext.setAttribute("pc", pageContext);
        %>
        
        ${s1.id} <br/>
        ${pc.session.id} <br/>
        ${pc['session']['id']} <br/>
        ${pc['session'].id} <br/>
        ${pc.session['id']} <br/>
    </body>
</html>
