<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="df" uri="http://utils.mycompany.com" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        getDate: ${ df:getDate() } <br/>
        
        <jsp:useBean id="d" class="java.util.Date" />
        addDays: ${ df:addDays(d, 2) } <br/>
        addDays: ${ df:addDays( df:getDate(), 2) } <br/>
        
        <hr/>
        ${df:createDate("d1", "request", pageContext)}
        
        ${d1} <br/>
        ${requestScope.d1} <br/>
        
    </body>
</html>
