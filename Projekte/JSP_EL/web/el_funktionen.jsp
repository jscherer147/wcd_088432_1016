<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="mf" uri="/WEB-INF/tlds/utils - eindeutiger String" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

<pre>
        1. public Java-Klasse

        2. public static Methode

        3. EL-Funktion in einer TLD-Datei deklarieren 
            (TLD im WEB-INF oder Unterordner von WEB-INF)

        4. Die Lib einbinden: taglib-Directive

        5. EL-Funktion aufrufen (immer mit dem Präfix)
</pre>
        
        12 + 12 = ${ mf:summe(12, 12) } <br>
        
        mf:summe(12, 12) = ${ mf:summe(12, 12) + 22 } <br>
        mf:summe(1.2, 1.2) = ${ mf:summe(1.2, 1.2) } <br>
        
        mf:summe(x, y) = ${ mf:summe(x, y) } <br>
        <%
            pageContext.setAttribute("x", 12);
            pageContext.setAttribute("y", "12");
        %>
        mf:summe(x, y) = ${ mf:summe(x, y) } <br>
        
        mf:summe(mf:summe(1, 2), 3) = ${ mf:summe(mf:summe(1, 2), 3) }
        
    </body>
</html>
