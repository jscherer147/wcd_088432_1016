<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <h1>pageContext: Das einzige impl. Objekt, das für Scriptlets gleich ist</h1>
        <h3>pageContext: eine Bean</h3>
        pageContext in Scriptlet: <%= pageContext %> <br/>
        \${pageContext} = ${pageContext} <br/>
        
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <h3>pageScope, requestScope, sessionScope, applicationScope:
             Maps mit den Attributen</h3>
        <% 
            request.setAttribute("att", "Donnerstag");
            pageContext.setAttribute("att", 12);
        %>
        
        \${att} = ${att} <br/>
        \${requestScope.att} = ${requestScope.att} <br/>
        
        <%
            pageContext.setAttribute("my.company", "My Company");
        %>
        \${ pageScope["my.company"] } = ${ pageScope["my.company"] } <br/>
        
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <h3>initParam: Map(String to String) mit den Context-Parameter</h3>
        \${initParam.firmenname} = ${initParam.firmenname} <br/>
        
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <h3>cookie: Map(String to Cookie)</h3>
        
        Cookie-Map: ${cookie} <br/>
        Cookie-Objekt: ${cookie.JSESSIONID} <br/>
        Cookie-Name: ${cookie.JSESSIONID.name} <br/>
        Cookie-Value: ${cookie.JSESSIONID.value} <br/>
        
        Kleine Aufgabe: <br/>
        JSESSIONID: ${pageContext.session.id} <br/>
        
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <h3>header und headerValues: Maps mit den Request-Headers</h3>
        
        <h4>header: Map (String to String)</h4>
        <h4>headerValues: Map (String to String[])</h4>
        
        <%--
        Einfache Map: \${header} = ${header} <br/> <br/>
        Komplette Map: ${headerValues} <br/>
        --%>
        
        Falsch: \${header.user-agent} = ${header.user-agent} <br/>
        \${header['user-agent']} = ${header['user-agent']} <br/>
        
        Array: ${headerValues['user-agent']} <br/>
        user-agent: ${headerValues['user-agent'][0]} <br/>
        
        
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <h3>param und paramValues: Maps mit den Request-Parameter</h3>
        
        <h4>param: Map (String to String)</h4>
        <h4>paramValues: Map (String to String[])</h4>
        
        <a href="impliziteObjekte.jsp?color=red&color=orange&size=big">sende Request-Parameter</a>
        <br/>
        
        red = ${param.color}<br/>
        red = ${paramValues.color[0]}<br/>
        orange = ${paramValues.color[1]}<br/>
        nichts = ${paramValues.color[2]}<br/>
        
        big = ${param.size} <br/>
        big = ${paramValues.size[0]} <br/>
        
    </body>
</html>
