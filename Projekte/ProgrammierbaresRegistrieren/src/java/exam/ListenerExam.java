package exam;

import javax.servlet.HttpConstraintElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletSecurityElement;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebListener;

@WebListener
public class ListenerExam implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        
        ServletContext context  = sce.getServletContext();
        
        ServletRegistration.Dynamic reg 
                = context.addServlet("JahresServlet", "exam.ServletJahr");
        
        reg.addMapping("/januar");
        
        HttpConstraintElement httpConstraintElement 
                = new HttpConstraintElement(ServletSecurity.EmptyRoleSemantic.DENY);
        ServletSecurityElement sse 
                = new ServletSecurityElement(httpConstraintElement);
        
        //http://docs.oracle.com/javaee/6/api/javax/servlet/ServletRegistration.Dynamic.html#setServletSecurity(javax.servlet.ServletSecurityElement)
        reg.setServletSecurity(sse);
        
        reg.addMapping("/februar");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

}
