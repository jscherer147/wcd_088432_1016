package listener;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;
import svts.ServletWoche;

@WebListener
public class ListenerRegistrator implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        ServletContext context = sce.getServletContext();

        ServletRegistration.Dynamic reg;

        //1.
        reg = context.addServlet("WocheKlein", "svts.ServletWoche");

        reg.addMapping("/mo");
        reg.addMapping("/di", "/mi");

        //2.
        reg = context.addServlet("WocheGross", ServletWoche.class);

        reg.addMapping("/MO");
        reg.addMapping("/DI", "/MI");

        //3
        
        //3.1
        //so werden Resource Injections nicht laufen:
        //Servlet servlet = new ServletWoche(); 
        //...

        //3.2
        try {
            Servlet servlet = context.createServlet(ServletWoche.class);
            reg = context.addServlet("WocheVollKlein", servlet);

            reg.addMapping("/montag", "/dienstag");
            
        } catch (ServletException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

}
