package web;

import java.util.List;
import java.util.Objects;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class User implements HttpSessionBindingListener {

    private String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        HttpSession session = event.getSession();
        ServletContext context = session.getServletContext();
        
//TODO: gleiche User verbieten
        synchronized(context) {
            List<User> userList = (List)context.getAttribute("userList");
            userList.add(this);
        }
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        
        HttpSession session = event.getSession();
        //User user = (User) session.getAttribute("user");
        
        ServletContext context = session.getServletContext();
        synchronized(context) {
            List<User> userList = (List)context.getAttribute("userList");
            userList.remove(this);
        }
        
    }
    
    
}
