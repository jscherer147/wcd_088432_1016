package svts;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletTestLongUrl extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("------> in der processRequest");
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletTestLongUrl</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>TestLongUrl</h1>");
            
            String url = "testLongUrl?p0=v0";
            
            StringBuilder sb = new StringBuilder(url);
            for (int i = 1; i < 1000; i++) {
                sb.append("&").append('p').append(i).append('=').append('v').append(i);
            }
            
            url = sb.toString();
            
            out.println("<a href=\"" + url 
                    + "\">aktualisieren und sehr viele Request-Parameter senden</a>");
            
            out.println("<br/>");
            int queryLen = url.split("\\?")[1].length();
            out.println("Query length: " + queryLen + " chars");
            
            out.println("<hr/>");
//            out.println("Parameter p999 = " + request.getParameter("p999"));
            
            out.println("</body>");
            out.println("</html>");
        } // out.close()
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
