package svts;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletLebenszyklus extends HttpServlet {

    //Im normalfall wird das Servlet erst beim 1. Request geladen und initialisiert.
    //Im DD kann man das Laden beim Deployen einstellen (load-on-startup)
    static {
        System.out.println("-- ServletLebenszyklus. static init (Klasse wird geladen)");
    }

    //------------------------------------------
    // Definieren nicht empfohlen:
    //------------------------------------------
    public ServletLebenszyklus() {
        System.out.println("-- ServletLebenszyklus. Constructor (Objekt wird erstellt)");
    }
    
    //------------------------------------------
    // Überschreiben nicht empfohlen:
    //------------------------------------------
    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("-- ServletLebenszyklus. init(ServletConfig) (Servlet wird initialisiert)");
        super.init(config); 
    }

    //------------------------------------------
    // überschreiben:
    //------------------------------------------
    @Override
    public void init() throws ServletException {
        System.out.println("-- ServletLebenszyklus. init() (Servlet wird initialisiert)");
        ServletConfig config = getServletConfig();
    }
    
    //------------------------------------------
    // Überschreiben nicht empfohlen:
    //------------------------------------------
    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        System.out.println("-- ServletLebenszyklus. service(ServletRequest, ServletResponse)");
        super.service(req, res);
    }

    //------------------------------------------
    // Überschreiben nicht empfohlen:
    //------------------------------------------
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("-- ServletLebenszyklus. service(HttpServletRequest, HttpServletResponse)");
        super.service(req, resp);
    }
    
    
    //------------------------------------------
    // überschreiben:
    //------------------------------------------
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("-- ServletLebenszyklus. doGet");
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletLebenszyklus</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Lebenszyklus</h1>");
            out.println("Wollen Sie, dass ein Servlet bereits beim deployen instanziiert und initialisiert wird: load-on-startup setzen");
            out.println("</body>");
            out.println("</html>");
        }
    }

    //------------------------------------------
    // überschreiben:
    //------------------------------------------
    @Override
    public void destroy() {
        System.out.println("-- ServletLebenszyklus. destroy()");
    }
    

}
