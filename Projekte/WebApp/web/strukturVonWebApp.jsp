<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

<pre>

    /WebApp/                    (Context Pfad)
        |-META-INF/
        |-WEB-INF/
            |-web.xml           (DD)
            |-mytaglib.tld      (TLD)
            |-lib/
                |-ext.lib1.jar
                |-ext.taglib.jar/
                    |-META-INF/
                        |-externaltaglib1.tld
                        |-tags/
                            |-TagFile1.tag
                            |-TagFile2.tag
                    |-p1/
                      |-TagHandler1.class
                      |-TagHandler2.class
            |-classes/
                |-p1/
                   |-MyClass1.class
                   |-MyClass2.class
                |-mytaglib/
                   |-MyTagHandler1.class
                   |-MyTagHandler2.class
            |-tags/
                |-lib1action1.tag
                |-lib1action2.tag
                |-lib2/
                    |-lib2action1.tag
                    |-lib2action2.tag
        |-pfade/
            |-bild.png
        |-index.jsp
</pre>        
        
    </body>
</html>
