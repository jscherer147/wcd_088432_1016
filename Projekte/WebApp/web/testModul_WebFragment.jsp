<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        database-server-ip aus dem Webfragment: ${initParam["database-server-ip"]}
        
        <hr/>
        
        <%--
            Exam: 
                Es gibt 3 Fragmente A, B, C.
                Wie kann man die Reihenfolge festlegen?
        
        Variante 1:
            <absolute-ordering>
                <name>A</name>
                <name>B</name>
                <name>C</name>
            </absolute-ordering>
    
        Variante 2:
            <absolute-ordering>
                <name>A</name>
                <others/>
                <name>C</name>
            </absolute-ordering>
    
        Variante 3:
            <absolute-ordering>
                <others/>
                <name>B</name>
                <name>C</name>
            </absolute-ordering>
    
        Variante 4:
            <absolute-ordering>
                <name>A</name>
                <name>B</name>
                <others/>
            </absolute-ordering>
    
        --%>
        
    </body>
</html>
