package svts;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import web.User;

public class ServletMainView extends HttpServlet {

    @Override
    public void init() throws ServletException {
        //Ich würde es so nicht machen.
        //Die init-Methode sollte die Daten für das aktuelle Servlet vorbereiten.
        //Hier bereitet sie die Liste auch für andere Servlets vor
        getServletContext().setAttribute("userList", new ArrayList<>());
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletMainView</title>");            
            out.println("</head>");
            out.println("<body>");
            
            HttpSession session = request.getSession(false);
                    
            if(session == null) {
            
                out.println("Sie sind nicht eingeloggt");
                out.println(" (<a href=\"form.html\">einloggen</a>)");
            
            } else {
                User user = (User)session.getAttribute("user");
                out.println("Hallo " + user.getName() +"!");
                out.println(" (<a href=\"logout\">ausloggen</a>)");
            }
            
            out.println("<h3>Eingeloggt:</h3>");
            
            ServletContext context = getServletContext();
            synchronized(context) {
                List<User> eingeloggteUser = (List) context.getAttribute("userList");
                for(User user : eingeloggteUser) {
                    out.println(user.getName() + "<br/>");
                }
            }
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
