package web.init;

import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Anwendungsinitializierer.
 * 
 * - Bereitet die Liste vor, die in allen Servlets verwendet wird
 * 
 */
public class InitApp implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        
        context.setAttribute("userList", new ArrayList<>());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

}
