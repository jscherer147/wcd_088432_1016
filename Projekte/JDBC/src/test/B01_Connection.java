package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class B01_Connection {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        
        //Manuell Treiber laden (selten nötig):
//        Class.forName("org.gjt.mm.mysql.Driver");
        
//        String url = "jdbc:mysql://localhost:3306/test_java";
        String url = "jdbc:derby://localhost:1527/test_java";
        
        try ( Connection connection = DriverManager.getConnection(url, "root", "1234") ) {
            System.out.println("Die Verbindung steht");
        }

        System.out.println("Verbindung geschlossen");
    }
    
}
