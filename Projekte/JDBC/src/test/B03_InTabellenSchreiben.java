package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class B03_InTabellenSchreiben {

    public static void main(String[] args) throws Exception {
        
        String sql = "INSERT INTO PERSONEN (VORNAME, NACHNAME) VALUES ('Jerry', 'M.')";
        
//        String url = "jdbc:mysql://localhost:3306/test_java";
        String url = "jdbc:derby://localhost:1527/test_java";
        
        try ( Connection connection = DriverManager.getConnection(url, "root", "1234") ) {
            
            try( Statement stm = connection.createStatement() ) {

                stm.executeUpdate(sql);
                
            } //close statement
        }//close connection        
        
    }
    
}
