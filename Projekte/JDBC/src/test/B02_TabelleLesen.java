package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class B02_TabelleLesen {

    public static void main(String[] args) throws Exception {
        
//        String url = "jdbc:mysql://localhost:3306/test_java";
        String url = "jdbc:derby://localhost:1527/test_java";
        
        try ( Connection connection = DriverManager.getConnection(url, "root", "1234") ) {
            
            try( Statement stm = connection.createStatement() ) {
                
                try ( ResultSet res = stm.executeQuery("select * from personen") ) {
                    
                    while( res.next() ) {
                        
                        String vorname = res.getString(1);
//                        String vorname = res.getString("vorname");
                        
//                        String nachname = res.getString(2);
                        String nachname = res.getString("nachname");
                        System.out.println("vorname: " + vorname + ", nachname: " + nachname);
                    }
                    
                } //close result set
            } //close statement
        }//close connection
        
    }
    
}
