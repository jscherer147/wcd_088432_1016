/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package svts;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apatrin
 */
public class ServletReadCookies extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletReadCookies</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Alle Cookies:</h1>");
            
            Cookie[] alleCookies = request.getCookies();
            
            if(alleCookies == null) {
                out.println("Keine Cookies gefunden");
            } else {
                
                for (Cookie c : alleCookies) {
                    
                    String name = c.getName();
                    String value = c.getValue();
                    
                    out.println(name + " = " + value + "<br/>");
                }
            }
            
            //request.getCookie("color") GIBT ES GAR NICHT

            //Exam: richtig nach dem Cookie 'color' suchen:
            String color = null;
            if( alleCookies!=null ) {
                
                for (Cookie c : alleCookies) {
                    if(c.getName().equals("color")) {
                        color = c.getValue();
                        break;
                    }
                }
            }
            
            out.println("<hr/>");
            out.println("color = " + color);
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
