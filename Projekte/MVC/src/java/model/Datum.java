package model;

import java.text.DateFormat;
import java.util.Date;

public class Datum {
    private final Date date;

    public Datum(Date date) {
        this.date = date;
    }
    
    public String getFormatiert() {
        return DateFormat.getDateTimeInstance().format(date);
    }
}
