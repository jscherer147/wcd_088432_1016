package svts;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Datum;

public class ServletController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //(Evtl. Erkennen, was der Client möchte)
        //1. Modell initialisieren/aktualisieren
        Datum d = new Datum(new Date());
        
        //2. An die View das Modell übergeben
        request.setAttribute("datum", d);
        
//        PrintWriter out = response.getWriter();
//        out.println("Zeile die im Controller entsteht");
//        out.flush();
        
        RequestDispatcher rd = request.getRequestDispatcher("/view");
        rd.forward(request, response); //weiterleiten
        
    }
}
