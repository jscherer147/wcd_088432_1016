package p1;

@interface MyAnn05 {
    //Primitive:
    int x() default 0;
    
    //Strings:
    String s() default "";
    
    //Class-Referenzen:
    Class<?> c() default Object.class;
    
    //Enums:
    MyEnum e() default MyEnum.V1;
    
    //Annotationen:
    Version v() default @Version(major = 1, minor=3);
    
    //Array von den oben genannten Typen
    String[] arrS() default {};
    Version[] arrV() default {};
}

@MyAnn05
class MyClass05_a{}

@MyAnn05()
class MyClass05_b{}

@MyAnn05(x=4)
class MyClass05_c{}

public class A05_AttributeDefault {

}
