package p1;

@interface MyAnn06a {
    String value();
}

@MyAnn06a(value = "v1")
class MyClass06a {}

@MyAnn06a("v1")
class MyClass06b {}

//-----------------------------------
@interface MyAnn06b {
    String value();
    int x() default 0;
}
//@MyAnn06b("v1", x=1)
@MyAnn06b(value = "v2", x=3)
class MyClass06c {}

@MyAnn06b("v2")
class MyClass06d {}



public class A06_value {

}
