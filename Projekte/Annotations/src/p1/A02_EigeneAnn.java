package p1;

//1. definieren:

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
//@Repeatable(...)
@interface MyAnn01 {}

//2. einsetzen
@MyAnn01
class MyClass01 {}

//3. Programm starten und nach der MyAnn01 sucht
public class A02_EigeneAnn {

    public static void main(String[] args) {
        
        Class<MyClass01> c1 = MyClass01.class;
        
        MyAnn01 a1 = c1.getAnnotation(MyAnn01.class);
        System.out.println("a1 = " + a1);
    }
    
}
