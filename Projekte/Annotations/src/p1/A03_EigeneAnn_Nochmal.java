package p1;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(value = RetentionPolicy.RUNTIME)
@interface Version {
    int major();
    int minor();
}

@Version(major=2, minor=3)
class MyPlugin {
    
}

public class A03_EigeneAnn_Nochmal {

    //Meine App:
    public static void main(String[] args) {
        
        Class<MyPlugin> clazz = MyPlugin.class;
        
        Version v = clazz.getAnnotation(Version.class);
        
        if( v==null ) {
            System.out.println("Fehler! Plugin hat keine Versionsangabe");
            return;
        }
        
        if( v.major() < 2 ) {
            System.out.println("Fehler! Plugin hat eine falsche Version");
            return;
        }
        
        System.out.println("Plugin geladen: v = " + v);
    }
    
}
