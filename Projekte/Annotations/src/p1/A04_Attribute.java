package p1;

enum MyEnum {
    V1, V2
}


@interface MyAnn04 {
    //Primitive:
    int x();
    
    //Strings:
    String s();
    
    //Class-Referenzen:
    Class<?> c();
    
    //Enums:
    MyEnum e();
    
    //Annotationen:
    Version v();
    
    //Array von den oben genannten Typen
    String[] arrS();
    Version[] arrV();
}


@MyAnn04(
    x = 2, 
    s = "Hallo", 
    c = Integer.class, 
    e = MyEnum.V1, 
    v = @Version(major = 1, minor = 2),

    arrS = { "mo", "mi" },
    arrV = {
        @Version(major = 1, minor = 2),
        @Version(major = 1, minor = 2),
        @Version(major = 1, minor = 2),
    }
)
public class A04_Attribute {

}
