/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ds;

import java.net.MalformedURLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apatrin
 */
public class HtmlFetcherTest {
    
    private static final String ADRESSE = "https://de.wikipedia.org/wiki/Liste_der_Gro%C3%9Fst%C3%A4dte_in_Deutschland";
    
    public HtmlFetcherTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("@BeforeClass");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("@AfterClass");
    }
    
    @Before
    public void setUp() {
        System.out.println("@Before");
    }
    
    @After
    public void tearDown() {
        System.out.println("@After");
    }

    @Test
    public void testFetch() throws Exception {
        
        String actual = HtmlFetcher.fetch(ADRESSE);
        
        assertNotNull( "Der geladene Text ist null", actual);
        assertFalse( actual.isEmpty() );
    }
    
    @Test(expected = MalformedURLException.class)
    public void testFetch_BadUrl() throws Exception {
        
        HtmlFetcher.fetch("asdfkjaslkdjaslkjd");
        
    }
    
    
}
