package ds;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;

public class TextIO {

    public static void save(String htmlText, Path file) throws IOException {
        
        try ( PrintWriter out = new PrintWriter( file.toFile() ) ) {
            out.write(htmlText);
        }
    }

    public static String load(Path file) throws IOException {
        
        try(BufferedReader in = new BufferedReader(new FileReader(file.toFile()))) {
            
            StringBuilder sb = new StringBuilder();
            
            String line;
            while( (line = in.readLine()) != null ) {
                sb.append(line).append("\n");
            }
            
            return sb.toString();
        }
        
    }
    
}
