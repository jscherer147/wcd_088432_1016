package ds.parse;

import ds.Stadt;
import java.util.List;

public interface WikiParser {
    
    public List<Stadt> parse(String htmlText);
    
}
