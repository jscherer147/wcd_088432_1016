package ds.parse;

import ds.Stadt;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class JsoupParser implements WikiParser {

    @Override
    public List<Stadt> parse(String htmlText) {
        List<Stadt> list = new ArrayList<>();
        
        Document document = Jsoup.parse(htmlText);
        
        Elements alleTabellen = document.getElementsByTag("table");
        
        Element tabelle = alleTabellen.get(2);
        
        Elements alleZeilen = tabelle.getElementsByTag("tr");
        
        for (int i = 1; i < alleZeilen.size()-1; i++) {
            Element zeile = alleZeilen.get(i);
            
            Stadt s = parse(zeile);
            list.add(s);
        }
        
        return list;
    }
    
    private Stadt parse(Element zeile) {
        
        Elements alleSpalten = zeile.getElementsByTag("td");
        
        Element spalteName = alleSpalten.get(0);
        String name = spalteName.getElementsByTag("a").get(0).ownText();
        
        Element spalteEinwohner = alleSpalten.get(9);
        String einwohnerText = spalteEinwohner.ownText();
        einwohnerText = einwohnerText.replace(".", "");
        int einwohner = Integer.parseInt(einwohnerText);
        
        Element spalteLand = alleSpalten.get(14);
        Elements alleAnchors = spalteLand.getElementsByTag("a");
        String land = alleAnchors.last().ownText();
        
        return new Stadt(name, einwohner, land);
    }

}
