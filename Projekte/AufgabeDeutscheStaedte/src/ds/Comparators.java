package ds;

import java.util.Comparator;

public class Comparators {

    public static final Comparator<Stadt> CMP_NAME = new Comparator<Stadt>() {
        @Override
        public int compare(Stadt s1, Stadt s2) {
            return s1.getName().compareTo(s2.getName());
        }
    };
    
    public static final Comparator<Stadt> CMP_LAND = (Stadt s1, Stadt s2) -> {
        return s1.getLand().compareTo(s2.getLand());
    };

    public static final Comparator<Stadt> CMP_EINWOHNER 
                            = (s1, s2) -> s1.getEinwohner() - s2.getEinwohner();

}
