package ds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class HtmlFetcher {

    public static String fetch(String address) throws IOException {
        
        URL url = new URL(address);
        
        try ( BufferedReader in = new BufferedReader( new InputStreamReader( url.openStream() ) ) ) {
            
            StringBuilder sb = new StringBuilder();
            
            String line;
            while( (line = in.readLine()) != null ) {
                sb.append(line).append("\n");
            }
            
            return sb.toString();
        }
    }
    
}
