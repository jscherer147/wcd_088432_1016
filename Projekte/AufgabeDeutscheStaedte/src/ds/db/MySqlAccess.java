package ds.db;

import ds.Stadt;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MySqlAccess implements DatabaseAccess {

    public MySqlAccess() throws ClassNotFoundException {
        Class.forName("org.gjt.mm.mysql.Driver");
    }
    
    private Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/test_java";
        return DriverManager.getConnection(url, "root", "1234");
    }
    
    private String loadSqlScript(String resFileName) throws IOException {
        InputStream is = MySqlAccess.class.getResourceAsStream(resFileName);
        
        try ( BufferedReader in = new BufferedReader(new InputStreamReader(is)) ) {
            String line;
            
            StringBuilder sb = new StringBuilder();
            while( (line = in.readLine()) != null ) {
                sb.append(line).append("\n");
            }
            
            return sb.toString();
        }
    }
    public void dropTable() throws SQLException, IOException {
        String sql = loadSqlScript("mysql-drop-table.sql");
        
        try(Connection connection = getConnection()) {
            try(Statement stm = connection.createStatement()) {
                stm.executeUpdate(sql);
            }
        }
    }
    
    public void createTable() throws SQLException, IOException {
        String sql = loadSqlScript("mysql-create-table.sql");
        
        try(Connection connection = getConnection()) {
            try(Statement stm = connection.createStatement()) {
                stm.executeUpdate(sql);
            }
        }
    }

    @Override
    public void insert(List<Stadt> list) throws SQLException, IOException {
        
        String sql = loadSqlScript("mysql-insert.sql");
        
        try(Connection connection = getConnection()) {
            try( PreparedStatement stm = connection.prepareStatement(sql) ) {

                for (Stadt stadt : list) {
                    
                    stm.setString(1, stadt.getName());
                    stm.setString(2, stadt.getLand());
                    stm.setInt(3, stadt.getEinwohner());
                    
                    //stm.executeUpdate(sql);
                    //stm.executeUpdate();
                    stm.addBatch();
                }
                
                stm.executeBatch();
            } //close statement
        }
    }

    @Override
    public List<Stadt> selectAll() throws SQLException, IOException {
        
        String sql = loadSqlScript("mysql-select-all.sql");
        
        List<Stadt> list = new ArrayList<>();
        
        try(Connection connection = getConnection()) {
            try(Statement stm = connection.createStatement()) {
                try(ResultSet res = stm.executeQuery(sql)) {
                    
                    while(res.next()) {
                        
                        String name = res.getString(1);
                        String land = res.getString(2);
                        int einwohner = res.getInt(3);

                        list.add(new Stadt(name, einwohner, land));
                    }
                }
            }
        }
        
        return list;
    }

    
    
}
