package ds.db;

import ds.Stadt;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface DatabaseAccess {
    
    void insert(List<Stadt> list) throws SQLException, IOException;
    List<Stadt> selectAll() throws SQLException, IOException;
    
}
