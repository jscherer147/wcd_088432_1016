package ds;

import ds.db.DatabaseAccess;
import ds.db.MySqlAccess;
import ds.parse.WikiParser;
import ds.parse.JsoupParser;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class RunApp {
    
    //https://de.wikipedia.org/wiki/Liste_der_Gro%C3%9Fst%C3%A4dte_in_Deutschland

    public static void main(String[] args) throws Exception {
        
        String htmlText;
        
        htmlText = HtmlFetcher.fetch("https://de.wikipedia.org/wiki/Liste_der_Gro%C3%9Fst%C3%A4dte_in_Deutschland");

        Path file = Paths.get("wiki.html");
        
        TextIO.save(htmlText, file);
        
        htmlText = TextIO.load(file);
        
        WikiParser parser = new JsoupParser();

        List<Stadt> list = parser.parse(htmlText);

        System.out.println("Geparst: " + list.size());
        
        
        //JDBC
        MySqlAccess mysqlAccess = new MySqlAccess();
        mysqlAccess.dropTable();
        mysqlAccess.createTable();
        //...
        
        DatabaseAccess access = mysqlAccess;
        
        access.insert(list);
        
        list = access.selectAll();
        
        for (Stadt stadt : list) {
            System.out.println(stadt);
        }
        
        System.out.println("aus einer Datenbank geladen: " + list.size());
        
    } // end of main
    
}
