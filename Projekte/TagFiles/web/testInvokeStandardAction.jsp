<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="mtf" tagdir="/WEB-INF/tags/" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <%--        
        <mtf:textHeadFoot head="Header-Footer Text">
            Der tolle dynamisch ausgewertete Text. Zeile 1<br/>
            Der tolle dynamisch ausgewertete Text. Zeile 2
        </mtf:textHeadFoot>
        
        <br/>
        --%>
            
        <mtf:textHeadFoot>
            <jsp:attribute name="head">
                Head Zeile 1. ${1 + 2}<br/>
                Head Zeile 2<br/>
            </jsp:attribute>
            <jsp:body>
                2. Der tolle dynamisch ausgewertete Text. Zeile 1<br/>
                2. Der tolle dynamisch ausgewertete Text. Zeile 2
            </jsp:body>
        </mtf:textHeadFoot>
    </body>
</html>
