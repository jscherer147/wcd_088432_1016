<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="mtf" tagdir="/WEB-INF/tags/" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <jsp:useBean id="d1" class="java.util.Date"/>
        <mtf:useVariable input="${d1}" x="3" y="4">
            JSP. Im Body der Action \${erg} = ${erg} <br/>
        </mtf:useVariable>
        
        JSP. \${pageScope.input} = ${pageScope.input} <br/>
        JSP. Nach der Action \${erg} = ${erg} <br/>
        
    </body>
</html>
