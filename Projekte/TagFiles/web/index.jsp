<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="mtf" tagdir="/WEB-INF/tags/" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <%--
        <jsp:include page="sayHello.jsp?userName=Tom"/>
        --%>
        
        <mtf:sayHello userName="Tom" />
        <hr/>


        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <c:set var="att" value="att aus der index.jsp"/>
        JSP. ${pageScope.att} <br>
        
        JSP. pageContext: <%= pageContext %> <br/>
        JSP. \${pageContext} = ${pageContext} <br/>
        <mtf:unterschiedeZuJsp/>
        
        JSP. ${pageScope.att} <br>

    </body>
</html>
