<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@attribute name="input" type="java.util.Date" rtexprvalue="true"%>
-- TagFile. \${pageScope.input} = ${pageScope.input} <br/>
-- TagFile. \${pageScope.input["class"]} = ${pageScope.input["class"]} <br/>

<%@attribute name="x" %>
<%@attribute name="y" %>

<%@variable name-given="erg" scope="AT_BEGIN" variable-class="java.lang.Long" %>
<c:set var="erg" value="${x + y}"/>
-- TagFile. ${x} + ${y} = ${erg} <br/>

<jsp:doBody/>