<%--
    **************************
    Directives
    **************************
--%>
<%--
    tag-Directive.

    page - Directive vs. tag-Directive.
    Anders als für page-Directive bei einer JSP: 
--%>
<%@tag body-content="empty" %>
<%@tag dynamic-attributes="mapMitDynAtts" %>

<%-- 
    page - Directive vs. tag-Directive.
    gleiche Attribute: 
--%>
<%@tag pageEncoding="UTF-8"%>
<%@tag import="java.util.Date"%>
<%@tag isELIgnored="false"%>

<%--
    attribute-Directive.
--%>
<%@attribute name="att" required="false" rtexprvalue="false" %>

<%--
    variable-Directive
--%>
<%@variable name-given="x" scope="NESTED" %>

<%-- 
    Weitere Directives wie bei einer JSP:

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="toInclude.txt" %>
--%>


<%--
    **************************
    Template Texte
    **************************
--%>
-- Hallo, ich bin ein Template Text. <!-- Ich auch -->

<%--
    **************************
    Scriptlets, Expressions, Declarations
    **************************
--%>
<% int x = 3; %>
<%= x + this.x %>
<%! int x = 44; %>
<br/>

<%--
    Gleich wie bei einer JSP für Scriptlets und Expressions:
    <%= application %>
    <%= session %>
    ...

    Anders als bei einer JSP für Scriptlets und Expressions: jspContext

    JSP. pageContext: <%= pageContext %> <br/>
    Tag-File. jspContext: <%= jspContext %> <br/>

--%>

-- Tag-File. jspContext: <%= jspContext %> <br/>

<%--
    **************************
    EL: wie bei einer JSP
    **************************
--%>
-- Tag-File. \${pageContext} = ${pageContext} <br/>

<h3>Tag-File hat eigenes Page-Scope!!!</h3>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="att" value="Bin att aus der Tag-Datei"/>
-- Tag-File. ${pageScope.att} <br/>


<%--
    **************************
    Standard-Actions
    **************************

    - Dieselben, die auch eine JSP hat
    <jsp:useBean...
    ...
    
    - Standard-Actions nur für Tag-Files:
    <jsp:doBody/>
    <jsp:invoke/>
--%>


