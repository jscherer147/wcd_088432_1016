package m19;

import java.net.URL;

public class TestJavaSeGetResource {

    public static void main(String[] args) {
        
        Class<?> clazz = TestJavaSeGetResource.class;
        
        //Name: absolut im CLASSPATH
        URL url = clazz.getResource("/m19/data/gedicht.txt");
        System.out.println("url: " + url);
        
        //Name: relativ zum Package
        url = clazz.getResource("data/gedicht.txt");
        System.out.println("url: " + url);
    }
    
}
