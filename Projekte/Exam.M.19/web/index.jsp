<%-- 
    Document   : index
    Created on : 18.10.2016, 12:49:16
    Author     : apatrin
--%>

<%@page import="java.net.URL"%>
<%@page import="m19.TestJavaSeGetResource"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <%
            Class<?> clazz = TestJavaSeGetResource.class;
            //Name: absolut im CLASSPATH
            URL url = clazz.getResource("/m19/data/gedicht.txt");
        %>
        url: <%=url%> <br/>
        
        <%
            //Name: relativ zum Package
            url = clazz.getResource("data/gedicht.txt");
        %>
        url: <%=url%> <br/>
        
        <hr/>
        
        <%
            //Name: absolut in der WebApp
            url = application.getResource("/WEB-INF/gedicht2.txt");
        %>
        url: <%=url%> <br/>
        
        
    </body>
</html>
