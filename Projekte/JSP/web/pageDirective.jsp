<%--
<jsp:directive.page />
--%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@page contentType="text/html" pageEncoding="UTF-8"%>
        
        <%@page import="java.util.Date" %>
        <%@page import="java.util.ArrayList" import="java.util.LinkedList" %>
        <%@page import="java.util.Set, java.util.Map" %>
                
        <%--
        <%@page errorPage="exeptionHandler.jsp" %>
        <%@page isErrorPage="true" %>
        --%>
        
        <%@page isELIgnored="true" %>
        ${1 + 1}
        
        <%@page session="true" %>
        
        
        <%@page isThreadSafe="false" %>
        implements javax.servlet.SingleThreadModel
        
    </body>
</html>
