<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Lebenszyklus</h1>
        
<pre>
    1. Anfrage:

    - Translieren (.jsp -> .java)
    - Compilieren (.java -> .class)
    
    - Servlet-Klasse laden
    - Servlet instanziieren
    - Servlet initialisieren: init(ServletConfig) -> jspInit()

    1. und alle weiteren Anfragen:
    - service(ServletRequest, ServletResponse) -> _jspService(HttpServletRequest, HttpServletResponse)

    Beim Runterfahren:
    - destroy() -> jspDestroy()
</pre>        

        <hr/>
        
        <%!
            static {
                System.out.println("Die Jsp-Servlet-Klasse wird geladen");
            }

            {
                System.out.println("Das Java-Servlet-Objekt wird erstellt");
            }

            public void jspInit() {
                System.out.println("Das Servlet wird initialisiert (jspInit)");
            }
        %>
        
        <%
            System.out.println("Das Servlet behandelt ein requst (_jspService)");
        %>
        
        <%!
            public void jspDestroy() {
                System.out.println("Das Servlet wird zerstört (jspDestroy)");
            }
        %>
    </body>
</html>

