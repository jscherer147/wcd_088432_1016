<%@page import="java.util.Enumeration"%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <h1>PageContext extends JspContext</h1>
        
        <h2>PageContext bringt den 'page scope'</h2>
        
        <%
            //application.setAttribute("att", new Date());
            pageContext.setAttribute("att", new Date(), PageContext.APPLICATION_SCOPE);
            
            //session.setAttribute("att", 26);
            pageContext.setAttribute("att", 26, PageContext.SESSION_SCOPE);
            
            //request.setAttribute("att", "Freitag");
            pageContext.setAttribute("att", "Freitag", PageContext.REQUEST_SCOPE);
                        
            pageContext.setAttribute("att", LocalDate.now());
            //pageContext.setAttribute("att", LocalDate.now(), PageContext.PAGE_SCOPE);
        %>




        <%
            session.removeAttribute("att");
            
            //Achtung! pageContext.removeAttribute(String) löscht in ALLEN Scopes!
            //pageContext.removeAttribute("att");
            
            //Lieber die nehmen:
            pageContext.removeAttribute("att", PageContext.PAGE_SCOPE);
        %>

        
        
        att (application): 
        <%= application.getAttribute("att") %> <br/>
        att (application): 
        <%= pageContext.getAttribute("att", PageContext.APPLICATION_SCOPE) %> <br/>
        
        att (session): 
        <%= session.getAttribute("att") %> <br/>
        att (session): 
        <%= pageContext.getAttribute("att", PageContext.SESSION_SCOPE) %> <br/>
        
        att (request): 
        <%= request.getAttribute("att") %> <br/>
        att (request): 
        <%= pageContext.getAttribute("att", PageContext.REQUEST_SCOPE) %> <br/>
        
        att (page): 
        <%= pageContext.getAttribute("att") %> <br/>
        att (page): 
        <%= pageContext.getAttribute("att", PageContext.PAGE_SCOPE) %> <br/>
        
        
        <hr/>
        
        <%
            Enumeration<String> alleNamen;
            
            alleNamen = application.getAttributeNames();
            alleNamen = session.getAttributeNames();
            alleNamen = request.getAttributeNames();
            
            alleNamen = pageContext.getAttributeNamesInScope(PageContext.PAGE_SCOPE);
            
            int scope = pageContext.getAttributesScope("att");
        %>
        
        <%-- Wichtig!!! --%>
        
        <%
            //Sucht zuerst im page-scope, danach im request, session und appliaction
            Object att = pageContext.findAttribute("att");
        %>
        
        att = <%= att %> 
        
    </body>
</html>
