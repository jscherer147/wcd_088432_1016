<%-- 
    Scripting-Kommentar
--%>

<%-- Directive --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Ich bin ein Template-Text</h1>
        
        
        <%-- Scriptlet: --%>
        <%
            int x = 3;
            x++;
        %>
        
        <%-- Expression: --%>
        x = <%= x %> <br/>
        time: <%= new java.util.Date().getTime() %> <br/>
        time: <%= new Date().getTime() %> <br/>
        
        
        y = <%= y %> <br/>
        
        <%-- Declaration --%>
        <%!
            int y = 7;
            
            static {
            }

            {
                
            }
            
            static void m1() {}
            void m2() {}
            
            class Inner {

            }
        %>
        
        
    </body>
</html>
