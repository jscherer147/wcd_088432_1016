<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <h1>getter-Methoden ergeben Properties einer JavaBean:</h1>
        
        servletContext: <%= pageContext.getServletContext() %> <br/>
        servletConfig: <%= pageContext.getServletConfig()%> <br/>
        ServletRequest request: <%= pageContext.getRequest() %> <br/>
        ServletResponse <%= pageContext.getResponse() %> <br/>
        session: <%= pageContext.getSession() %> <br/>
        exception: <%= pageContext.getException() %> <br/>
        JspWriter out: <%= pageContext.getOut() %> <br/>
        
    </body>
</html>
