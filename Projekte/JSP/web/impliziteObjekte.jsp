<%@page import="java.io.PrintWriter"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <h1>Implizite Objekte</h1>
        
        
        <h2>request und response</h2>
        z.B. response: <%= response %>
        
        
        <h2>ServletContext application</h2>
        
        <% 
            //in einem Servlet:
            ServletContext v1 = getServletContext(); 
        %>
        
        application: <%= application %>
        
        
        <h2>ServletConfig config</h2>
        <%
            //in einem Servlet
            ServletConfig v2 = getServletConfig();
        %>
        
        config: <%= config %>
        
        
        <h2>HttpSession session (falls page-Directive es nicht deaktiviert)</h2>
        <%@page session="true" %>
        
        <%
            //in einem Servlet
            //HttpSession v = request.getSession();
        %>
        
        session: <%= session %>
        
        
        <h2>JspWriter out</h2>
        
        <%
            //in einem Servlet:
            PrintWriter v3 = response.getWriter();
            v3.print("Zeile 1 (falsch für eine Jsp, da direkt mit dem PrintWriter)<br/>");
        %>
        
        Zeile 2 (richtig, mit dem JspWriter)<br/>
        
        out: <%= out %>
        
        
        <h2>exception (nur bei den Error-Handler sinnvoll)</h2>
        <%@page isErrorPage="true" %>
        
        exception: <%= exception %>
        
        
        <h2>Object page = this</h2>
        Nicht in der Prüfung
        
        
        <h2>PageContext pageContext</h2>
        
        In einem Servlet gibt es nicht ähnliches
        
        
    </body>
</html>
