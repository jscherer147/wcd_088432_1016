package wwm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Game {

    private final List<Question> questions;
    private int currentQuestionIndex;
    private boolean aborted;
    private final List<Joker> jokers;
    private final Rules rules;
    
    public Game(List<Question> questions, Rules rules) {
        this.questions = questions;
        this.rules = rules;
        
        jokers = new ArrayList<>( Arrays.asList(Joker.values()) );
    }

    public boolean isJokerAvailable() {
        return !jokers.isEmpty();
    }

    public List<Joker> getJokers() {
        return jokers;
    }
    
    public void useJocker(int index) {
        if( index >= jokers.size() || index < 0 ) {
            throw new IllegalGameStateException("Bad joker index: " + index);
        }
        
        if(getCurrentQuestion().isAnswered()) {
            throw new IllegalGameStateException("Question is already answered. Joker usage not possible");
        }
        
        Joker j = jokers.remove(index);
        j.use(getCurrentQuestion());
    }
    
    public Question getCurrentQuestion() {
        return questions.get(currentQuestionIndex);
    }

    public void setAborted(boolean aborted) {

        if (isOver()) {
            throw new IllegalGameStateException("Das beendete Spiel kann nicht verlassen werden");
        }

        this.aborted = aborted;
    }

    public boolean isAborted() {
        return aborted;
    }

    public boolean isLastQuestion() {
        return currentQuestionIndex == rules.getQuestionsPerGame() - 1;
    }

    public boolean isWon() {
        return isLastQuestion() && isAnsweredAndAnsweredCorrect();
    }

    public boolean isLost() {
        return isAnsweredAndAnsweredWrong();
    }

    public boolean isOver() {
        return isAborted() || isWon() || isLost();
    }

    public void setUserAnswerChoice(int indexAnswer) {
        getCurrentQuestion().setUserChoice(indexAnswer);
    }

    public Question nextQuestion() throws IllegalGameStateException {

        if (!getCurrentQuestion().isAnswered()) {
            throw new IllegalGameStateException("Current question not answered");
        }

        if (isOver()) {
            throw new IllegalGameStateException("Game is over");
        }

        currentQuestionIndex++;

        return getCurrentQuestion();
    }

    public int getCurrentAmount() {
        return rules.getAmount(currentQuestionIndex+1);
    }
    
    public int getMaxAmount() {
        return rules.getAmount( rules.getQuestionsPerGame() );
    }
    
    public int getGaranteedAmount() {
        if(isAnsweredAndAnsweredCorrect())
                return rules.getGaranteedAmount(currentQuestionIndex+1);

        return rules.getGaranteedAmount(currentQuestionIndex);
    }
    
    public int getAmountOnAbort() {
        
        if(isAnsweredAndAnsweredCorrect())
            rules.getAmount(currentQuestionIndex+1);

        return rules.getAmount(currentQuestionIndex);
    }
    
    /**
     * Achtung! Wenn die Frage noch gar nicht beantwortet wurde, liefert die Methode false
     * @return 
     */
    private boolean isAnsweredAndAnsweredCorrect() {
        Question q = getCurrentQuestion();

        if (q.isAnswered()) {
            int indexUserAnswer = q.getIndexUserChoiceAnswer();
            int indexCorrectAnswer = q.getIndexCorrectAnswer();

            return indexCorrectAnswer==indexUserAnswer;
        }
        
        return false;
    }
    /**
     * Achtung! Wenn die Frage noch gar nicht beantwortet wurde, liefert die Methode false
     * @return 
     */
    private boolean isAnsweredAndAnsweredWrong() {
        Question q = getCurrentQuestion();

        if (q.isAnswered()) {
            int indexUserAnswer = q.getIndexUserChoiceAnswer();
            int indexCorrectAnswer = q.getIndexCorrectAnswer();

            return indexCorrectAnswer!=indexUserAnswer;
        }
        
        return false;
    }
    
}
