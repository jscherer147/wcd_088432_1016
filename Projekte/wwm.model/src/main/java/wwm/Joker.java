package wwm;

import java.util.List;
import java.util.Random;

public enum Joker {
    J50 {
        public String getFormatName() {
            return "50:50";
        }
        @Override
        public void use(Question frage) {
            strikeoutWrongAnswers(frage);
        }
    }, 
    PHONE {
        public String getFormatName() {
            return "Call a friend";
        }
        @Override
        public void use(Question frage) {
            Question.Answer a = findCorrectAnswer(frage, 75);
            a.setJokerPhone(true);
        }
    }, 
    AUDIENCE {
        public String getFormatName() {
            return "Ask audience";
        }
        @Override
        public void use(Question frage) {
            Question.Answer a = findCorrectAnswer(frage, 95);
            a.setJokerAudience(true);
        }
    };
    
    public String getFormatName() {
        return name();
    }
    
    public void use(Question frage) {
    }
    
    private static void strikeoutWrongAnswers(Question q){
        List<Question.Answer> answers = q.getAnswers();
        int countWrong = answers.size()/2;
        
        for (Question.Answer a : answers) {
            if(a.isJoker50())
                throw new IllegalGameStateException("Für die Frage wurde bereits der 50:50 Joker eingesetzt. Frage: " + q);
        }
        
        Random r = new Random();
        for (int i = 0; i < countWrong; i++) {
            int index = r.nextInt(answers.size());
            Question.Answer a = answers.get(index);

            if(a.isCorrect() || a.isJoker50()) {
                i--;
                continue;
            }
            
            a.setJoker50(true);
        }
    }
    
    private static Question.Answer findCorrectAnswer(Question q, int chance){
        if(chance<0 || chance>100)
            throw new IllegalGameStateException("Bad chance to get the correct answer: " + chance + ". Possible values: 0 ... 100");
            
        Random r = new Random();
        
        if(r.nextInt(100) < chance) {
            int index = q.getIndexCorrectAnswer();
            return q.getAnswer(index);
        }
        
        Question.Answer a = null;
        
        do {
            a = q.getRandomWrongAnswer();
        } while( a.isJoker50() );
        
        return a;
    }
}
