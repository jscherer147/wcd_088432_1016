package wwm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Question implements Cloneable {

    public static class Answer {

        private final String text;
        private boolean userChoice;
        private final boolean correct;
        private boolean joker50;
        private boolean jokerPhone;
        private boolean jokerAudience;

        public Answer(String text, boolean correct) {
            this.text = text;
            this.correct = correct;
        }

        public void setJoker50(boolean joker50) {
            this.joker50 = joker50;
        }

        public boolean isJoker50() {
            return joker50;
        }

        public boolean isJokerPhone() {
            return jokerPhone;
        }

        public void setJokerPhone(boolean jokerPhone) {
            this.jokerPhone = jokerPhone;
        }

        public boolean isJokerAudience() {
            return jokerAudience;
        }

        public void setJokerAudience(boolean jokerAudience) {
            this.jokerAudience = jokerAudience;
        }
        
        public final String getText() {
            return text;
        }

        public final boolean isCorrect() {
            return correct;
        }

        private void setUserChoice(boolean userWahl) {
            this.userChoice = userWahl;
        }

        public boolean isUserChoice() {
            return userChoice;
        }

        @Override
        public String toString() {
            return "Answer (correct: " + correct + ", user choice: " + userChoice + ")";
        }
    }

    //------------------------------------
    private final int category;
    private final String text;

    private final List<Answer> answers = new ArrayList<>();

    public Question(int category, String text) {
        this.category = category;
        this.text = text;
    }

    @Override
    public final Question clone() {
        Question q = new Question(category, text);

        for (Answer a : answers) {
            q.addAnswer(a.getText(), a.isCorrect());
        }

        return q;
    }

    public void setUserChoice(int answerIndex) {

        for (int i = 0; i < answers.size(); i++) {
            Answer a = answers.get(i);

            if (a.isUserChoice() && i != answerIndex) {
                throw new IllegalGameStateException("Another answer is already choosen");
            }
        }

        answers.get(answerIndex).setUserChoice(true);
    }

    public int getIndexUserChoiceAnswer() {
        for (int i = 0; i < answers.size(); i++) {
            Answer a = answers.get(i);

            if (a.isUserChoice()) {
                return i;
            }
        }

        return -1;
    }

    public int getIndexCorrectAnswer() {
        for (int i = 0; i < answers.size(); i++) {
            Answer a = answers.get(i);

            if (a.isCorrect()) {
                return i;
            }
        }

        throw new IllegalGameStateException("No correct answer found for question: " + this);
    }

    public boolean isAnswered() {
        for (Answer a : answers) {
            if (a.isUserChoice()) {
                return true;
            }
        }

        return false;
    }

    public void addAnswer(String text, boolean correct) throws IllegalGameStateException {
        if (answers.size() >= Rules.DEFAULT.getAnswersPerQuestion()) {
            throw new IllegalGameStateException("Can't add answer. Too many answers for question: " + this);
        }

        answers.add(new Answer(text, correct));
    }

    public int getCategory() {
        return category;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public Answer getAnswer(int index) {
        return answers.get(index);
    }

    public Answer getRandomAnswer() {
        return getAnswer(new Random().nextInt(answers.size()));
    }
    
    public Answer getRandomWrongAnswer() {
        Answer a = null;
        
        do {
            a = getAnswer(new Random().nextInt(answers.size()));
        } while(a.isCorrect());
        
        return a;
    }

    public void shuffleAnswers() {
        Collections.shuffle(answers);
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {

        String postfix = text; //text.length() > 7 ? text.substring(0, 7) + "..." : text;
        return "Question. [C:" + category + ", A:" + answers.size() + " / " + postfix + "]";
    }
}
