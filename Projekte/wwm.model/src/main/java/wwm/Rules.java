package wwm;

import java.util.ArrayList;
import java.util.List;

public class Rules {

    public static final Rules DEFAULT = new Rules();
    
    public class Level {
        private final int nr;
        private final int amount;
        private final boolean garanteed;

        public Level(int nr, int summe, boolean garanteed) {
            this.nr = nr;
            this.amount = summe;
            this.garanteed = garanteed;
        }

        public int getNumber() {
            return nr;
        }

        public int getAmount() {
            return amount;
        }

        public boolean isGaranteed() {
            return garanteed;
        }
    }
    
    //----------------------------------------------
    private final List<Level> levels = new ArrayList<>();
    
    //----------------------------------------------
    private Rules() {
        levels.add(new Level(1, 50, false));
        levels.add(new Level(2, 100, false));
        levels.add(new Level(3, 200, false));
        levels.add(new Level(4, 300, false));
        levels.add(new Level(5, 500, true));
        
        levels.add(new Level(6, 1000, false));
        levels.add(new Level(7, 2000, false));
        levels.add(new Level(8, 4000, false));
        levels.add(new Level(9, 8000, false));
        levels.add(new Level(10, 16000, true));
        
        levels.add(new Level(11, 32000, false));
        levels.add(new Level(12, 64000, false));
        levels.add(new Level(13, 125000, false));
        levels.add(new Level(14, 500000, false));
        levels.add(new Level(15, 1000000, true));
    }
    
    public int getQuestionsPerGame() {
        return 15;
    }
    
    public int getAnswersPerQuestion() {
        return 4;
    }

    public List<Level> getLevels() {
        return levels.subList(0, getQuestionsPerGame());
    }

    /*
    *  Stufen 1 ... N.
    */
    public Level getLevel(int index) {
        if(index>levels.size())
            throw new IllegalGameStateException("Wrong level index: " + index);
        
        return levels.get(index-1);
    }
    
    /**
     * 
     * @param level 1 ... N. Für level 0 liefert 0
     * @return 
     */
    public int getAmount(int level) {
        if(level==0)
            return 0;
        
        return getLevel(level).getAmount();
    }

    public int getGaranteedAmount(int stufe) {
        for(int i=stufe; i>=1; i--) {
            Level gs = getLevel(i);
            
            if(gs.isGaranteed())
                return gs.getAmount();
        }
        
        return 0;
    }
    
}
