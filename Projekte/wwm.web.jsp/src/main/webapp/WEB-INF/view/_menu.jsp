<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="header">
    <h1>Wer wird Millionär</h1>

    <c:if test="${empty game or game.over}" >
        <c:url var="neuGameUrl" value="new" />
        <a href="${neuGameUrl}">neues Spiel starten</a>
    </c:if>
        
    <hr>
</div>