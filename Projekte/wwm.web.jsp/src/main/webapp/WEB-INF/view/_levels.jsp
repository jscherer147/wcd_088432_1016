<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="levels">
    <table>
        <tr>
            <td>Stufe</td>
            <td>Betrag</td>
        </tr>          

        <jsp:useBean id="rules" type="wwm.Rules" scope="application"/>
        <c:forEach items="${rules.levels}" var="level">
            
            <jsp:useBean id="level" type="wwm.Rules.Level" />
            
            <tr>
                <c:choose>
                    <c:when test="${level.garanteed}">
                        <c:set var="bgColor" value="orange" />
                    </c:when>
                    <c:otherwise>
                        <c:set var="bgColor" value="white" />
                    </c:otherwise>
                </c:choose>            
                <td style="background-color:${bgFarbe}">
                    ${level.number} ${level.number==param.currentLevel ? '*' : ''}
                </td>
                <td style="background-color:${bgColor}">${level.amount}</td>

            </tr>                    
        </c:forEach>

    </table>
</div>