<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="mfmt" uri="/WEB-INF/tlds/format" %>


<jsp:useBean id="game" type="wwm.Game" scope="session"/>

<c:set var="question" value="${game.currentQuestion}" />
<jsp:useBean id="question" type="wwm.Question" />

<!DOCTYPE html>
<html>
    <jsp:include page="_head.jsp"/>

    <body>
        <jsp:include page="_menu.jsp"/>
        <div id="main">

            <mfmt:amount value="${game.currentAmount}" var="currentAmount" />
            <h2>Frage ${question.category} (${currentAmount})</h2>
            ${question.text} <br>

            <c:forEach items="${question.answers}" var="answer" varStatus="status">
                <jsp:useBean id="answer" type="wwm.Question.Answer" />
                
                <c:choose>
                    <c:when test="${answer.correct}">
                        <c:set var="answerColor" value="green" />
                    </c:when>
                    <c:when test="${answer.userChoice}">
                        <c:set var="answerColor" value="red" />
                    </c:when>
                    <c:otherwise>
                        <c:set var="answerColor" value="black" />
                    </c:otherwise>
                </c:choose>            

                ${status.count}. <span style="color:${answerColor}">${answer.text}</span><br>

            </c:forEach>

            <hr>
            <c:choose>
                <c:when test="${game.aborted}">
                    <mfmt:amount value="${game.amountOnAbort}" var="amountOnAbort" />
                    <h2>Sie haben das Spiel verlassen. Sie nehmen ${amountOnAbort} mit</h2>
                </c:when>
                <c:when test="${game.won}">
                    <mfmt:amount value="${game.maxAmount}" var="maxAmount" />
                    <h2>Sie haben das Spiel gewonnen! Gewinn: ${maxAmount}</h2>
                </c:when>
                <c:when test="${game.lost}">
                    <mfmt:amount value="${game.garanteedAmount}" var="garanteedAmount" />
                    <h2>Sie haben das Spiel verloren. Sie nehmen ${garanteedAmount} mit</h2>
                </c:when>
                <c:otherwise>
                    <c:if test="${!game.lastQuestion}">
                        <c:url var="url" value="question"/>
                        <a href="${url}">Zu der Frage ${question.category+1}</a>
                    </c:if>
                </c:otherwise>
            </c:choose>
        </div>

        <jsp:include page="_levels.jsp">
            <jsp:param name="currentLevel" value="${question.category}" />
        </jsp:include>
    </body>
</html>
