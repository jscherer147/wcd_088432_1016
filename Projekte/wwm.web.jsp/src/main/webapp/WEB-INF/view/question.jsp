<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="mfmt" uri="/WEB-INF/tlds/format" %>

<jsp:useBean id="game" type="wwm.Game" scope="session" />

<c:set var="question" value="${game.currentQuestion}" />
<jsp:useBean id="question" type="wwm.Question"/>

<c:set var="answers" value="${question.answers}" />

<!DOCTYPE html>
<html>
    <jsp:include page="_head.jsp"/>

    <body>
        <jsp:include page="_menu.jsp"/>

        <div id="main">
            <mfmt:amount value="${game.currentAmount}" var="currentAmount" />
            <h2>Frage ${question.category} (${currentAmount})</h2>
            ${question.text} <br>

            <c:forEach items="${answers}" var="answer" varStatus="status">
                <jsp:useBean id="answer" type="wwm.Question.Answer" />

                ${status.count}. 
                <c:choose>
                    <c:when test="${answer.joker50}">
                        <span style="text-decoration: line-through;">${answer.text}</span>
                        (Joker 50:50)
                    </c:when>
                    <c:otherwise>
                        <c:url var="url" value="answer">
                            <c:param name="index" value="${status.count-1}"/>
                        </c:url>
                        <a href="${url}">${answer.text}</a>
                    </c:otherwise>
                </c:choose>

                ${answer.jokerPhone ? "(Joker Freund anrufen) " : ""}
                ${answer.jokerAudience ? "(Joker Publikum) " : ""}

                <br>
            </c:forEach>

            <hr>
            <c:url var="url" value="abort" />
            <mfmt:amount value="${game.amountOnAbort}" var="amountOnAbort" />
            <a href="${url}">Spiel verlassen (${amountOnAbort} mitnehmen)</a><br>

            <mfmt:amount value="${game.garanteedAmount}" var="garanteedAmount" />
            Sicher: ${garanteedAmount}<br>

            <c:if test="${game.jokerAvailable}">
                <h3>Joker:</h3>

                <c:forEach items="${game.jokers}" var="joker" varStatus="statusJ">
                    <c:url var="url" value="joker">
                        <c:param name="joker" value="${statusJ.count-1}"/>
                    </c:url>
                    ${statusJ.count}. <a href="${url}">${joker.formatName}</a><br>
                </c:forEach>

            </c:if>

        </div>

        <jsp:include page="_levels.jsp">
            <jsp:param name="currentLevel" value="${question.category}" />
        </jsp:include>

    </body>
</html>
