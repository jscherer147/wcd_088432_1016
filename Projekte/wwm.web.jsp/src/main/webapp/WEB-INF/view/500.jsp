<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page isErrorPage="true" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Fehler!</h1>

        <c:choose>
            <%-- Achtung! exception ist kein implizites Objekt für EL! --%>
            <c:when test="${not empty exception}">
                <h3>${exception.message}</h3>
            </c:when>
            <c:otherwise>
                --Debug--<br>
                Exception (impl. Obj): <%=exception%><br>
            </c:otherwise>
        </c:choose>

        <br>
        <a href="back">zurück</a>
    </body>
</html>
