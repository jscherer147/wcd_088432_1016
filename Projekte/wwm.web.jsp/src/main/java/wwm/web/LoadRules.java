package wwm.web;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import wwm.Rules;

public class LoadRules implements ServletContextListener{

    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("rules", Rules.DEFAULT);
    }

    public void contextDestroyed(ServletContextEvent sce) {
    }

}
