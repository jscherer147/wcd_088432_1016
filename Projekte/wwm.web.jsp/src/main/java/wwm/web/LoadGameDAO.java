package wwm.web;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import wwm.Question;
import wwm.data.GameDAO;
import wwm.data.memory.MemoryDAO;
import wwm.data.utils.CsvParser;

public class LoadGameDAO implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        try (InputStream is = sce.getServletContext().getResourceAsStream("/WEB-INF/WWM.csv")) {

            Map<Integer, List<Question>> questionPool = CsvParser.parseFromStream(is, "ISO-8859-1");
            GameDAO instance = new MemoryDAO( questionPool );
            
//            Class.forName("com.mysql.jdbc.Driver");
//            GameDAO instance = new MySqlDAO();

            sce.getServletContext().setAttribute("gameDAO", instance);
        } catch (Exception e) {
            throw new RuntimeException(e);

        }

    }

    public void contextDestroyed(ServletContextEvent sce) {
    }

}
