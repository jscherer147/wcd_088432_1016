package wwm.web;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class FormatAmount extends SimpleTagSupport {
    
    public static String format(int summe, Locale locale) {
        return NumberFormat.getCurrencyInstance(locale).format(summe);
    }
    
    //---------------------------------------------------
    private int value;
    private String var;

    public void setValue(int value) {
        this.value = value;
    }

    public void setVar(String var) {
        this.var = var;
    }
    
    @Override
    public void doTag() throws JspException, IOException {
        
        PageContext context = (PageContext)getJspContext();

        //bug in TomCat 8.0: Locale ist 'de' und nicht 'de_DE'
        //Locale locale = context.getRequest().getLocale();
        Locale locale = Locale.getDefault();
        
        String erg = format(value, locale);
        
        if(var!=null) {
            context.setAttribute(var, erg);
        } else {
            context.getOut().print(erg);
        }
    }
}
