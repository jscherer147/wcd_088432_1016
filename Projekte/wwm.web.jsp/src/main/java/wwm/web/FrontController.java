package wwm.web;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import wwm.Game;
import wwm.Question;
import wwm.Rules;
import wwm.data.GameDAO;

/**
 *
 * @author apatrin
 */
public class FrontController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String viewDir = "/WEB-INF/view/";
        String target;
        String pathInfo = request.getPathInfo();

        try {
            if ("/welcome".equals(pathInfo)) {
                target = viewDir + "welcome.jsp";

            } else if ("/new".equals(pathInfo)) {
                newGame(request);
                target = viewDir + "question.jsp";

            } else if ("/question".equals(pathInfo)) {
                nextQuestion(request);
                target = viewDir + "question.jsp";

            } else if ("/answer".equals(pathInfo)) {
                setUserAnswerChoice(request);
                target = viewDir + "answer.jsp";

            } else if ("/abort".equals(pathInfo)) {
                abortGame(request);
                target = viewDir + "answer.jsp";

            } else if ("/joker".equals(pathInfo)) {
                useJoker(request);
                target = viewDir + "question.jsp";
            } else if ("/back".equals(pathInfo)) {
                target = getViewForCurrentModelState(request);
                target = response.encodeRedirectURL(target);
                response.sendRedirect(target);
                return;
            } else if ("/reloadAnswer".equals(pathInfo)) {
                target = viewDir + "answer.jsp";

            } else if ("/reloadQuestion".equals(pathInfo)) {
                target = viewDir + "question.jsp";

            } else {
                target = viewDir + "404.jsp";
            }

        } catch (Exception e) {

            request.setAttribute("exception", e);
            target = viewDir + "500.jsp";
        }

        request.getRequestDispatcher(target).forward(request, response);

    }

    private String getViewForCurrentModelState(HttpServletRequest request) {
        Game game = null;

        try {
            game = findGame(request);
        } catch (IllegalWebStateException e) {
            return "welcome";
        }

        if (game.isOver()) {
            return "reloadAnswer";
        }

        if (game.getCurrentQuestion().isAnswered()) {
            return "answer";
        }

        return "reloadQuestion";
    }

    private Game findGame(HttpServletRequest request) throws IllegalWebStateException {
        HttpSession session = request.getSession(false);

        if (session == null) {
            throw new IllegalWebStateException("Session not found");
        }

        Game game = (Game) session.getAttribute("game");

        if (game == null) {
            throw new IllegalWebStateException("Game not found in Session-Scope");
        }

        return game;
    }

    private boolean isGameRunning(HttpServletRequest request) {
        Game game = null;
        try {
            game = findGame(request);
            
        } catch (IllegalWebStateException e) {
            return false;
        }
        
        return game!=null && !game.isOver();
    }
    
    private void newGame(HttpServletRequest request) {

        if (isGameRunning(request)) {
            throw new IllegalWebStateException("You have a running game");
        }

        Rules rules = (Rules) getServletContext().getAttribute("rules");
        GameDAO gameDAO = (GameDAO) getServletContext().getAttribute("gameDAO");

        List<Question> questions = gameDAO.createQuestionSet();

        Game game = new Game(questions, rules);

        HttpSession session = request.getSession();

        synchronized (session) {
            session.setAttribute("game", game);
        }
    }

    private void nextQuestion(HttpServletRequest request) {
        synchronized (request.getSession()) {
            Game game = findGame(request);
            game.nextQuestion();
        }
    }

    private void abortGame(HttpServletRequest request) {
        synchronized (request.getSession()) {
            Game game = findGame(request);
            game.setAborted(true);
        }
    }

    private void setUserAnswerChoice(HttpServletRequest request) {
        synchronized (request.getSession()) {
            Game game = findGame(request);

            String paramIndex = request.getParameter("index");

            int indexAnswer;

            try {
                indexAnswer = Integer.parseInt(paramIndex);
                game.setUserAnswerChoice(indexAnswer);
            } catch (NumberFormatException e) {
                throw new IllegalWebStateException("Bad index value for user answer choice: " + paramIndex);
            }
        }
    }

    private void useJoker(HttpServletRequest request) {
        synchronized (request.getSession()) {
            Game game = findGame(request);
            String paramJoker = request.getParameter("joker");

            try {
                int indexJoker = Integer.parseInt(paramJoker);
                game.useJocker(indexJoker);

            } catch (NumberFormatException e) {
                throw new IllegalWebStateException("Bad value for parameter joker: " + paramJoker);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
