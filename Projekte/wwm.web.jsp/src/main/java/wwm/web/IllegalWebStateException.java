package wwm.web;

public class IllegalWebStateException extends IllegalStateException {

    public IllegalWebStateException(String errMsg) {
        super(errMsg);
    }
    
}
