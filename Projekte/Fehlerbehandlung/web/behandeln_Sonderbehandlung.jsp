<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%-- Das implizite Objekt 'exception' für Scriptlets wird aktiviert: --%>
<%@page isErrorPage="true" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Sonderbehandlung für die JSPs mit page-Directive mit errorPage</h1>
        
        Das geht nur mit isErrorPage=true: <br/>
        exception: <%= exception %> <br/>
        
        Das geht auch OHNE isErrorPage=true: <br/>
        exception: <%= pageContext.getException() %> <br/>
        exception: ${pageContext.exception} <br/>
        
    </body>
</html>
