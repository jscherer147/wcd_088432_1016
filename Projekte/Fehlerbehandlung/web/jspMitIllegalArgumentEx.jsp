<%-- 
    Document   : jspMitIllegalArgumentEx
    Created on : 19.09.2016, 12:45:08
    Author     : apatrin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <%
            if(true)
                throw new IllegalArgumentException("test IllegalArgumentEx");
        %>
        
    </body>
</html>
