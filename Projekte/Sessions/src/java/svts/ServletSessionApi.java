package svts;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletSessionApi extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletSessionApi</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>ServletSessionApi</h1>");

//            HttpSession session = request.getSession();
            HttpSession session = request.getSession(true);
            //1. Der Container versucht den Client zu identifizieren:
            //   - Nach dem Cookie JSESSIONID sichen
            //   - Oder nach dem Wert der JSESSIONID in der URL
            
            //2.a Falls JSESSIONID nicht gefunden wird:
            //   - Neue JSESSIONID generieren
            //   - Set-Cookie Header im Response setzen:
            //          Set-Cookie: JSESSIONID=A59C0966B0D0A30BA7290947B76A816B; Path=/Sessions/; HttpOnly
            //   - Das neue HttpSession-Objekt erzeugen
            //   - Das HttpSession-Objekt dem gerade erstellten JSESSIONID zuordnen
            //   - Das HttpSession-Objekt zurückliefern
            
            //2.b JSESSIONID gefunden:
            //   - Das alte HttpSession-Objekt finden und zurückliefern
            
            //url-rewriting:
            String url = "sessionApi";
            url = response.encodeURL(url);
            
            out.println("<a href=\"" + url + "\">aktualisieren</a>");
            
            
            out.println("<hr/>");

            //*******************************
            //Die Attributen-Api
            session.getAttribute("att");
            session.setAttribute("att", "22");
            session.removeAttribute("att");

            
            //*******************************
            // allgemein
            String sessionId = session.getId();
            out.println("sessionId: " + sessionId + "<br/>");
            out.println("isNew: " + session.isNew() + "<br/>");
            
            long creationTime = session.getCreationTime(); //Timestampt
            out.println("creationTime: " + creationTime + " Millisekunden</br>");
            
            long lastAccessedTime = session.getLastAccessedTime();
            out.println("lastAccessedTime: " + lastAccessedTime + " Millisekunden<br/>");
            
                    
            //*******************************
            //Timeout - Methoden
            int maxInactiveInterval = session.getMaxInactiveInterval();
            out.println("maxInactiveInterval: " + maxInactiveInterval + " Sek.<br/>");
            
            session.setMaxInactiveInterval(10);
            
            //*******************************
            //session beenden
            session.invalidate();
            
            
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    public ServletSessionApi() {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
