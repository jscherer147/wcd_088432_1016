/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package svts;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author apatrin
 */
public class ServletCounter extends HttpServlet {

    private int countGesamt = 0;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletCounter</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>ServletCounter</h1>");
            
            synchronized(this) {
                countGesamt++;
            }
            out.println("Gesamtanzahl aller Anfragen aller Clients: " + countGesamt + "<br/>");

            
            //*********************************************************
            //*********************************************************
            //*********************************************************
            
            HttpSession session = request.getSession();
            
            synchronized(session) {
                Integer countClient = (Integer)session.getAttribute("countClient");
                if(countClient==null) {
                    countClient = 0;
                }

                countClient++;
                session.setAttribute("countClient", countClient);
                
                out.println("Gesamtanzahl Ihrer Anfragen: " + countClient + "<br/>");
            }
            
            out.println("<hr/>");
            out.println("<a href=\"count\">aktualisieren</a>");
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
