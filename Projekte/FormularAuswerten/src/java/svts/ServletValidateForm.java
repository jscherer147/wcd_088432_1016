package svts;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletValidateForm extends HttpServlet {

    private void printForm(HttpServletResponse response, String userName, String errMsg) throws IOException{
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletForm</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<form action=\"validateForm\" method=\"post\">");
            out.println("Name: <input type=\"text\" name=\"userName\" value=\"" + userName + "\"/>");
            out.println("<input type=\"submit\"/>");
            
            if(errMsg!=null) {
                out.println("<h3>" + errMsg + "</h3>");
            }
            
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        printForm(response, "", null);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String userName = request.getParameter("userName");
        
        if(userName==null || userName.length() < 5) {
            userName = userName == null ? "" : userName;
            printForm(response, userName, "Bitte den Namen eingeben (mind. 5 Zeichen)");
            return;
        }
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletValidateForm</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Hallo " + userName + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

}
