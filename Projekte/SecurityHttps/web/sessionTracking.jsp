<%@page import="java.util.EnumSet"%>
<%@page import="java.util.Set"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        Session-ID: ${pageContext.session.id} <br/>
        
        <c:url var="target" value="sessionTracking.jsp"/>
        <a href="${target}">aktualisieren mit url-rewriting</a> <br/>
        <a href="sessionTracking.jsp">aktualisieren OHNE url-rewriting</a> <br/>
        
        <hr/>

        <%
            Set<SessionTrackingMode> estm = application.getEffectiveSessionTrackingModes();
            Set<SessionTrackingMode> dstm = application.getDefaultSessionTrackingModes();
        %>
        
        <h3>EffectiveSessionTrackingModes</h3>
        
        <c:forEach items="${pageContext.servletContext.effectiveSessionTrackingModes}" var="mode" >
            ${mode} 
        </c:forEach>
        <br>
        
        <h3>DefaultSessionTrackingModes</h3>
        
        <c:forEach items="${pageContext.servletContext.defaultSessionTrackingModes}" var="mode" >
            ${mode} 
        </c:forEach>
        <br>
        
        <hr/>
        
        <%--
        <session-config>
            <session-timeout>
                30
            </session-timeout>
            <tracking-mode>SSL</tracking-mode>
        </session-config>
        
        ODER in einem ServletContextListener in seiner contextInitialized Methode:
        
            Set<SessionTrackingMode> stm = EnumSet.of(SessionTrackingMode.SSL);
            application.setSessionTrackingModes(stm);
        --%>
        
    </body>
</html>
