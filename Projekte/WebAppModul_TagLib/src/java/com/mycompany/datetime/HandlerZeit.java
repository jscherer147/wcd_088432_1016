package com.mycompany.datetime;

import java.io.IOException;
import java.time.LocalTime;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class HandlerZeit extends SimpleTagSupport {

    @Override
    public void doTag() throws JspException, IOException {
        
        LocalTime time = LocalTime.now();
        getJspContext().getOut().print(time);
        
    }

    
}
