<%@page import="javax.annotation.Resource"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <%!
            @Resource(name = "numberOfTopics")
            private Integer numberOfTopics;
        %>
        
        numberOfTopics: <%= numberOfTopics %>
        

        <h1>Exam:</h1>
        
        <%!
            @Resource(name = "x")
            private Integer hallo;
            @Resource(name = "y")
            private Integer y;
        %>
        
        x = <%= hallo %> <br/>
        y = <%= y %> <br/>
        
    </body>
</html>
