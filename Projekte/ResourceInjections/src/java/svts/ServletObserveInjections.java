package svts;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/observeInjections")
public class ServletObserveInjections extends HttpServlet {
    
    @Resource(name = "jdbc/TestDB")
    private DataSource ds1;
    
    private DataSource ds2;

    @Resource(name = "jdbc/TestDB2")
    private void setDs2(DataSource ds2) {
        System.out.println("--> injectDs2. ds2: " + ds2);
        this.ds2 = ds2;
    }
    
    
    @PostConstruct
    private void nachInjections() {
        System.out.println("--> nachInjections. ds1: " + ds1);
    }
    
    @Override
    public void init() throws ServletException {
        //hier sind die Injections bereits durchgeführt
        System.out.println("--> init");
    }

    @Override
    public void destroy() {
        System.out.println("--> destroy. ds1: " + ds1);
    }
    
    @PreDestroy
    private void nachDerDestroy() {
        System.out.println("--> nachDerDestroy. ds1: " + ds1);
    }
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletObserveInjections</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>ServletObserveInjections</h1>");
            
            try(Connection c1 = ds1.getConnection()) {
                out.println("Connection mit ds1: " + c1 + "<br/>");
            } catch(SQLException e) {
                out.println("Fehler für Connection mit ds1: " + e.getMessage() + "<br/>");
            }
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
