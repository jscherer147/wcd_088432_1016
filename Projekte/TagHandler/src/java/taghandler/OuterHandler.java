package taghandler;

import java.io.IOException;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class OuterHandler extends SimpleTagSupport {

    @Override
    public void doTag() throws JspException, IOException {
        
        JspContext jspContext = getJspContext();
        JspWriter out = jspContext.getOut();
        
        out.println("Outer " + this + "<br/>");
        
        JspFragment jspBody = getJspBody();
        if(jspBody!=null) {
            jspBody.invoke(null);
        }
        
    }
    
}
