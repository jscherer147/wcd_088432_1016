package mycore;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class ForEachHandler extends SimpleTagSupport{
    
    private String var;
    private Iterator<?> iterator;
        
    public void setVar(String var) {
        this.var = var;
    }
    
    public void setItems(Object items) {
        
        if( items instanceof Object[] ){
            
            Object[] arr = (Object[])items;
            Collection<?> coll = Arrays.asList(arr);
            iterator = coll.iterator();
            
        } else if( items instanceof Collection ) {
            Collection<?> coll = (Collection)items;
            iterator = coll.iterator();
            
        } else if( items instanceof Map ) {
            Collection<?> coll = ((Map)items).entrySet();
            iterator = coll.iterator();
            
        } else if (items instanceof Iterator ) {
            iterator = (Iterator)items;
            
        } else if( items instanceof String ) {
            String str = (String)items;
            String[] arr = str.split(",");
            Collection<?> coll = Arrays.asList(arr);
            iterator = coll.iterator();
            
        } else if (items instanceof Enumeration<?> ) {
            
            Enumeration<?> en = (Enumeration)items;
            
            iterator = new Iterator<Object>() {
                @Override
                public boolean hasNext() {
                    return en.hasMoreElements();
                }

                @Override
                public Object next() {
                    return en.nextElement();
                }
            };
            
        } else {
            throw new IllegalArgumentException("Der Container-Typ wird nicht unterstützt: " + items.getClass());
        }
    }

    @Override
    public void doTag() throws JspException, IOException {
        
        JspContext jspContext = getJspContext();
        
        while( iterator.hasNext() ) {
            Object element = iterator.next();
            
            if(var!=null) {
                jspContext.setAttribute(var, element);
            }
            
            JspFragment body = getJspBody();
            if(body!=null) {
                body.invoke(null);
            }
        }
        
        if(var!=null) {
            jspContext.removeAttribute(var);
        }
        
    }
    
    
    
}
