package mycore;

import java.io.IOException;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class IfHandler extends SimpleTagSupport {

    private boolean test;
    private String var;
    private int scope = PageContext.PAGE_SCOPE;

    public void setTest(boolean test) {
        this.test = test;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public void setScope(String scope) {

        switch (scope) {
            case "page":
                break;
            case "request":
                this.scope = PageContext.REQUEST_SCOPE;
                break;
            case "session":
                this.scope = PageContext.SESSION_SCOPE;
                break;
            case "application":
                this.scope = PageContext.APPLICATION_SCOPE;
                break;
            default:
                throw new IllegalArgumentException("Bad scope " + scope);
        }
    }

    @Override
    public void doTag() throws JspException, IOException {
        if (var != null) {
            JspContext jspContext = getJspContext();
            jspContext.setAttribute(var, test, scope);
        }

        if (test) {

            JspFragment body = getJspBody();

            if (body != null) {
                body.invoke(null);
            }
        }

    }

}
