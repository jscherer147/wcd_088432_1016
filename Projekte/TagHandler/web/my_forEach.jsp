<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="mc" uri="/WEB-INF/tlds/mycore" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <%
            String[] arr1 = { "mo", "di", "mi" };
            pageContext.setAttribute("arr1", arr1);
        %>
        
        <c:forEach items="${arr1}" var="x">
            ${x} 
        </c:forEach>
        <br/>
        <mc:forEach items="${arr1}" var="x">
            ${x} 
        </mc:forEach>
        
        <hr/>
        <c:forEach items="${header}" var="x">
            ${x.key} 
        </c:forEach>
        <br/>
        <mc:forEach items="${header}" var="x">
            ${x.key} 
        </mc:forEach>
        
        <hr/>

        <c:forEach items="${pageContext.request.headerNames}" var="headerName">
            ${headerName} 
        </c:forEach>
        <br/>
        
        <mc:forEach items="${pageContext.request.headerNames}" var="headerName">
            ${headerName} 
        </mc:forEach>
        <br/>
        
    </body>
</html>
