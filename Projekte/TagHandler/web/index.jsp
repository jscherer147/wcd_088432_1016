<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="mth" uri="/WEB-INF/tlds/myhandler" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <mth:outer>
            <mth:inner/>
        </mth:outer>
        
        <hr/>
        
        <mth:produkt name="Milch" e101="8" e200="22" e707="33" />
        
    </body>
</html>
