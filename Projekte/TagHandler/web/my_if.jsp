<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="mc" uri="/WEB-INF/tlds/mycore" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <c:if test="true">
            1. c:if <br/>
        </c:if>

        <mc:if test="true">
            1. mc:if <br/>    
        </mc:if>

        <c:if test="false">
            2. c:if <br/>
        </c:if>

        <mc:if test="false">
            2. mc:if <br/>    
        </mc:if>

        <c:if test="${1+1==2}">
            3. c:if <br/>
        </c:if>

        <mc:if test="${1+1==2}">
            3. mc:if <br/>    
        </mc:if>

        <c:if test="${1+1==2}" var="erg">
            4. c:if. erg (page scope): ${pageScope.erg}<br/>
        </c:if>
            4. c:if. Nach dem Body: erg (page scope): ${pageScope.erg}<br/>
        <c:remove var="erg"/>

        <mc:if test="${1+1==2}" var="erg">
            4. mc:if. erg (page scope): ${pageScope.erg}<br/>
        </mc:if>
            4. mc:if. Nach dem Body: erg (page scope): ${pageScope.erg}<br/>

            
        <c:if test="false" var="erg" scope="request">
        </c:if>
            5. c:if. Nach dem Body: erg (request scope): ${requestScope.erg}<br/>
            <c:remove var="erg"/>
            
        <mc:if test="false" var="erg" scope="request">
        </mc:if>
            5. mc:if. Nach dem Body: erg (request scope): ${requestScope.erg}<br/>
            
            
    </body>
</html>
