package wwm.data.mysql;

import wwm.data.GameDAO;
import wwm.data.DatabaseException;
import wwm.data.BadDatabaseException;
import wwm.data.ResourceException;
import wwm.data.SqlScript;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import wwm.Question;
import wwm.Rules;

public class MySqlDAO implements GameDAO {

    public static Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://localhost:3306";
        return DriverManager.getConnection(url, "root", "1234");
    }

    public static int dropDatabase() throws SQLException, IOException {
        String script = SqlScript.load(MySqlDAO.class, "dropDatabase.sql");

        try (Connection connection = getConnection()) {
            try (Statement stm = connection.createStatement()) {
                return stm.executeUpdate(script);
            }
        }
    }

    public static int createDatabase() throws SQLException, IOException {
        String script = SqlScript.load(MySqlDAO.class, "createDatabase.sql");

        try (Connection connection = getConnection()) {
            try (Statement stm = connection.createStatement()) {
                return stm.executeUpdate(script);
            }
        }
    }

    public static int createTables() throws SQLException, IOException {
        String script = SqlScript.load(MySqlDAO.class, "createTables.sql");

        try (Connection connection = getConnection()) {
            try (Statement stm = connection.createStatement()) {
                return stm.executeUpdate(script);
            }
        }
    }

    //**************************************************************************
    @Override
    public List<Question> createQuestionSet() {
        List<Question> list = new LinkedList<>();

        try {
            for (int i = 0; i < Rules.DEFAULT.getQuestionsPerGame(); i++) {
                list.add( getRandomQuestion(i + 1) );
            }
        } catch (IOException e) {
            throw new ResourceException("Can't load mysql script", e);
        } catch (SQLException e) {
            throw new DatabaseException("Database access error", e);
        }

        return list;
    }

    private Question getRandomQuestion(int category) throws SQLException, IOException {
        String script = SqlScript.load(MySqlDAO.class, "selectRandomQuestion.sql");

        script = script.replace("${category}", String.valueOf(category));

        try (Connection connection = getConnection()) {
            try (Statement stm = connection.createStatement()) {

                ResultSet res = stm.executeQuery(script);
                
                if(!res.first()) {
                    throw new BadDatabaseException("No question of category " + category + " found");
                }

                Question q = new Question(res.getInt("category"), res.getString("text"));

                q.addAnswer(res.getString("correctAnswer"), true);
                q.addAnswer(res.getString("wrongAnswer1"), false);
                q.addAnswer(res.getString("wrongAnswer2"), false);
                q.addAnswer(res.getString("wrongAnswer3"), false);

                q.shuffleAnswers();
                
                return q;
            }
        }
    }

    public int insertQuestion(Question q) throws SQLException, IOException {
        String script = SqlScript.load(MySqlDAO.class, "insertQuestion.sql");

        try (Connection connection = getConnection()) {
            try (PreparedStatement stm = connection.prepareStatement(script)) {
//                addBatch(stm, q);
                return stm.executeUpdate();
            }
        }
    }

    public void insertQuestions(List<Question> listQuestions) throws SQLException, IOException {

        String script = SqlScript.load(MySqlDAO.class, "insertQuestion.sql");

        try (Connection connection = getConnection()) {
            try (PreparedStatement stm = connection.prepareStatement(script)) {

                int cnt = 0;
                for (Question q : listQuestions) {

                    if (cnt % 10 == 0) {
                        stm.executeBatch();
                        stm.clearBatch();
                    }

                    addBatch(stm, q);
                }

                stm.executeBatch();
            }
        }
    }

    private void addBatch(PreparedStatement stm, Question q) throws SQLException {

        String correctAnswer = q.getAnswer(q.getIndexCorrectAnswer()).getText();

        stm.setInt(1, q.getCategory());
        stm.setString(2, q.getText());
        stm.setString(3, correctAnswer);

        int i = 4;
        for (Question.Answer a : q.getAnswers()) {
            if (!a.isCorrect()) {
                stm.setString(i++, a.getText());
            }
        }

        stm.addBatch();
    }

}
