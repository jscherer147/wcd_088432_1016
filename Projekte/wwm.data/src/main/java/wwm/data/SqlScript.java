package wwm.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class SqlScript {

    public static String load(Class classFromResPackage, String resorceFileName) throws IOException {

        InputStream is = classFromResPackage.getResourceAsStream(resorceFileName);
        if (is == null) {
            throw new IOException("Resource not found: " + resorceFileName);
        }

        StringBuilder sb = new StringBuilder();
        String line;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"))) {
            while ((line = in.readLine()) != null) {
                sb.append(line).append("\n");
            }
        }

        return sb.toString();
    }
}
