package wwm.data;

import java.util.List;
import wwm.Question;

public interface GameDAO {
    public List<Question> createQuestionSet();
}
