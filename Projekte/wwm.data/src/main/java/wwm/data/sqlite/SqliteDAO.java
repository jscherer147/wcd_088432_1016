package wwm.data.sqlite;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import wwm.Question;
import wwm.Rules;
import wwm.data.BadDatabaseException;
import wwm.data.DatabaseException;
import wwm.data.GameDAO;
import wwm.data.ResourceException;
import wwm.data.SqlScript;

public class SqliteDAO implements GameDAO {

    private final Path databaseFile;

    public SqliteDAO(String databaseFileName) {
        this.databaseFile = Paths.get(databaseFileName).toAbsolutePath();
    }

    private String getUrl() {
        return "jdbc:sqlite:" + databaseFile;
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(getUrl());
    }

    public void dropDatabase() throws IOException {
        if (Files.exists(databaseFile, LinkOption.NOFOLLOW_LINKS)) {
            Files.delete(databaseFile);
        }
    }

    public void createDatabase() throws IOException, SQLException {
        String script = SqlScript.load(getClass(), "createTables.sql");

        try (Connection connection = getConnection()) {
            try (Statement stm = connection.createStatement()) {
                stm.executeUpdate(script);
            }
        }
    }

    public void insertQuestions(List<Question> listQuestions) throws SQLException, IOException {

        String script = SqlScript.load(getClass(), "insertQuestion.sql");

        try (Connection connection = getConnection()) {
            try (PreparedStatement stm = connection.prepareStatement(script)) {

                int cnt = 0;
                for (Question q : listQuestions) {

                    if (cnt % 10 == 0) {
                        stm.executeBatch();
                        stm.clearBatch();
                    }

                    addBatch(stm, q);
                }

                stm.executeBatch();
            }
        }
    }

    private void addBatch(PreparedStatement stm, Question q) throws SQLException {

        String correctAnswer = q.getAnswer(q.getIndexCorrectAnswer()).getText();

        stm.setInt(1, q.getCategory());
        stm.setString(2, q.getText());
        stm.setString(3, correctAnswer);

        int i = 4;
        for (Question.Answer a : q.getAnswers()) {
            if (!a.isCorrect()) {
                stm.setString(i++, a.getText());
            }
        }

        stm.addBatch();
    }

    @Override
    public List<Question> createQuestionSet() {
        List<Question> list = new LinkedList<>();

        try {
            for (int i = 0; i < Rules.DEFAULT.getQuestionsPerGame(); i++) {
                list.add(getRandomQuestion(i + 1));
            }
        } catch (IOException e) {
            throw new ResourceException("Can't load sqlite script", e);
        } catch (SQLException e) {
            throw new DatabaseException("Database access error", e);
        }

        return list;
    }

    private Question getRandomQuestion(int category) throws SQLException, IOException {
        String script = SqlScript.load(getClass(), "selectRandomQuestion.sql");

        script = script.replace("${category}", String.valueOf(category));

        try (Connection connection = getConnection()) {
            try (Statement stm = connection.createStatement()) {

                ResultSet res = stm.executeQuery(script);

                if (!res.next()) {
                    throw new BadDatabaseException("No question of category " + category + " found");
                }

                Question q = new Question(res.getInt("category"), res.getString("text"));

                q.addAnswer(res.getString("correctAnswer"), true);
                q.addAnswer(res.getString("wrongAnswer1"), false);
                q.addAnswer(res.getString("wrongAnswer2"), false);
                q.addAnswer(res.getString("wrongAnswer3"), false);

                q.shuffleAnswers();

                return q;
            }
        }
    }


}
