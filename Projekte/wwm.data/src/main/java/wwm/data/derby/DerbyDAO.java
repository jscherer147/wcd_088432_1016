package wwm.data.derby;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import wwm.Question;
import wwm.Rules;
import wwm.data.BadDatabaseException;
import wwm.data.DatabaseException;
import wwm.data.GameDAO;
import wwm.data.ResourceException;
import wwm.data.SqlScript;

public class DerbyDAO implements GameDAO {

    private String dbLocalPath;

    public DerbyDAO(String dbFileName) throws SQLException {
        this.dbLocalPath = dbFileName;
    }
    
    public int createDatabase() throws SQLException, IOException {
        String url = "jdbc:derby:" + dbLocalPath + ";create=true";
        DriverManager.getConnection(url).close();
        
        String script = SqlScript.load(DerbyDAO.class, "createTables.sql");

        try (Connection connection = getConnection()) {
            try (Statement stm = connection.createStatement()) {
                return stm.executeUpdate(script);
            }
        }
    }
    
    public void dropDatabase() throws SQLException, IOException {
        
        try {
            String url = "jdbc:derby:" + dbLocalPath + ";shutdown=true";
            Connection c = DriverManager.getConnection(url);
            throw new IllegalStateException("Cannot shutdown JavaDB server");
        } catch(SQLException e) {
            //correct shutdown causes SQLException
        } 
        
        Files.walkFileTree(Paths.get(dbLocalPath), new FileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.deleteIfExists(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                System.out.printf("Warning (visitFileFailed). File: %s / Error: %s%n", file, exc.getMessage());
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.deleteIfExists(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }
    
    private Connection getConnection() throws SQLException {
        String url = "jdbc:derby:" + dbLocalPath;
        return DriverManager.getConnection(url);    
    }

    public void insertQuestions(List<Question> listQuestions) throws SQLException, IOException {
        String script = SqlScript.load(DerbyDAO.class, "insertQuestion.sql");
        
        try (Connection connection = getConnection()) {
            try (PreparedStatement stm = connection.prepareStatement(script)) {

                int cnt = 0;
                for (Question q : listQuestions) {

                    if (cnt % 10 == 0) {
                        stm.executeBatch();
                        stm.clearBatch();
                    }

                    addBatch(stm, q);
                }

                stm.executeBatch();
            }
        }
    }
    
    private void addBatch(PreparedStatement stm, Question q) throws SQLException {

        String correctAnswer = q.getAnswer(q.getIndexCorrectAnswer()).getText();

        stm.setInt(1, q.getCategory());
        stm.setString(2, q.getText());
        stm.setString(3, correctAnswer);

        int i = 4;
        for (Question.Answer a : q.getAnswers()) {
            if (!a.isCorrect()) {
                stm.setString(i++, a.getText());
            }
        }

        stm.addBatch();
    }
    
    private Question getRandomQuestion(int category) throws SQLException, IOException {
        String script = SqlScript.load(DerbyDAO.class, "selectRandomQuestion.sql");

        script = script.replace("${category}", String.valueOf(category));

        try (Connection connection = getConnection()) {
            try (Statement stm = connection.createStatement()) {

                ResultSet res = stm.executeQuery(script);
                
                if(!res.next()) {
                    throw new BadDatabaseException("No question of category " + category + " found");
                }

                Question q = new Question(res.getInt("category"), res.getString("text"));

                q.addAnswer(res.getString("correctAnswer"), true);
                q.addAnswer(res.getString("wrongAnswer1"), false);
                q.addAnswer(res.getString("wrongAnswer2"), false);
                q.addAnswer(res.getString("wrongAnswer3"), false);

                q.shuffleAnswers();
                
                return q;
            }
        }
    }
    
    @Override
    public List<Question> createQuestionSet() {
        List<Question> list = new LinkedList<>();

        try {
            for (int i = 0; i < Rules.DEFAULT.getQuestionsPerGame(); i++) {
                list.add( getRandomQuestion(i + 1) );
            }
        } catch (IOException e) {
            throw new ResourceException("Can't load mysql script", e);
        } catch (SQLException e) {
            throw new DatabaseException("Database access error", e);
        }

        return list;
    }

}
