package wwm.data.utils;

import java.util.List;
import java.util.Map;
import java.util.Scanner;
import wwm.Question;
import wwm.data.mysql.MySqlDAO;

public class CreateAndFillMySqlDatabase {

    public static void main(String[] args) throws Exception {

        System.out.println("You are about to override a database 'wwm'. Sure (y)?");
        
        if( new Scanner(System.in).nextLine().charAt(0)!='y' ) {
            System.out.println("Nothing done");
            return;
        }
        
        System.out.println("Please wait...");
        MySqlDAO.dropDatabase();
        MySqlDAO.createDatabase();
        MySqlDAO.createTables();

        System.out.println("Parsing source file...");
        Map<Integer, List<Question>> map = CsvParser.parseFromResource("WWM.csv", "ISO-8859-1", CsvParser.class);

        int i=1;
        for (List<Question> listQuestions : map.values()) {
            MySqlDAO instance = new MySqlDAO();
            System.out.printf("Insert %d questions for category %d%n", listQuestions.size(), i++);
            instance.insertQuestions(listQuestions);
        }
        System.out.println("Done.");
    }

}
