package wwm.data.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import wwm.Question;
import wwm.Rules;

public class CsvParser {

    public static Map<Integer, List<Question>> parseFromStream(InputStream is, String charSet)
            throws UnsupportedEncodingException, IOException {
        List<Question> questionsWithOneCorrectAnswer = parse(is, charSet);
        return createCategoryMap(questionsWithOneCorrectAnswer);
    }
    
    public static Map<Integer, List<Question>> parseFromResource(String resource, String charSet, Class clazz)
            throws UnsupportedEncodingException, IOException {
        try( InputStream is = clazz.getResourceAsStream(resource) ) {
            return parseFromStream(is, charSet);
        }
    }
    
    private static List<Question> parse(InputStream is, String charSet) throws UnsupportedEncodingException, IOException  {
        if(is==null)
            throw new IllegalArgumentException("Error on parse questions (InputStream is null)");
        
        List<Question> listQuestions = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(is, charSet))) {

            String line;

            while ((line = in.readLine()) != null) {
                listQuestions.add( parse(line) );
            }
        }

        return listQuestions;
    }

    private static Question parse(String line) {
        String[] subStrings = line.split(";");

        int category = Integer.parseInt(subStrings[0]);
        String questionText = subStrings[1];

        questionText = questionText.replace("\"\"", "\"");

        if (questionText.startsWith("\"\"") && questionText.endsWith("\"?\"")) {
            questionText = questionText.substring(1, questionText.length() - 1);
        }

        if (questionText.startsWith("\"") && questionText.endsWith("\"?\"")) {
            questionText = questionText.substring(1, questionText.length() - 1);
        }

        if (questionText.startsWith("\"") && questionText.endsWith("?\"")) {
            questionText = questionText.substring(1, questionText.length() - 1);
        }

        questionText = questionText.trim();

        String correctAnswerText = subStrings[2];

        Question q = new Question(category, questionText);
        q.addAnswer(correctAnswerText, true);

        return q;
    }

    private static Map<Integer, List<Question>> createCategoryMap(List<Question> questionsWithOneCorrectAnswer)
            throws IllegalArgumentException {
        
        Map<Integer, List<Question>> mapQuestions = new TreeMap<>();
        
        Integer newCategory = null;
        List<Question> questions = null;
        
        for (Question q : questionsWithOneCorrectAnswer) {
            
            Integer questionCategory = q.getCategory();
            
            if(newCategory == null || !newCategory.equals(questionCategory)) {
                newCategory = questionCategory;
                questions = new LinkedList<>();

                mapQuestions.put(newCategory, questions);
            }
            
            questions.add(q);
        }
        
        //Zufällige falsche Antworten hinzufügen:
        for (Integer category : mapQuestions.keySet()) {
            
            List<Question> questionsForCategory = mapQuestions.get(category);
                    
            if( questionsForCategory.size() < Rules.DEFAULT.getAnswersPerQuestion() ) {
                throw new IllegalArgumentException("Not enough source questions for category " + category + " to build random wrong answers. Questions: " + questionsForCategory.size());
            }
            
//TODO: einzigartige falsche Antworten in einem Set sammeln
            for (int indexThis=0; indexThis<questionsForCategory.size(); indexThis++) {
                Question qThis = questionsForCategory.get(indexThis);
                
                Random rand = new Random();
                
                for(int countAnswer=0; countAnswer<Rules.DEFAULT.getAnswersPerQuestion()-1; countAnswer++) {
                    int indexOther = rand.nextInt(questionsForCategory.size());
                    
                    if(indexOther==indexThis) {
                        countAnswer--;
                        continue;
                    }
                    
                    Question qOther = questionsForCategory.get(indexOther);
                    Question.Answer answerOther = qOther.getRandomAnswer();
                    qThis.addAnswer(answerOther.getText(), false);
                }
            }
        }

        
        return mapQuestions;
    }
    
}
