package wwm.data.memory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import wwm.Question;
import wwm.Rules;
import wwm.data.GameDAO;

/**
 *
 * @author apatrin
 */
public class MemoryDAO implements GameDAO {
    
    private final Map<Integer, List<Question>> questionPool;
    
    public MemoryDAO(Map<Integer, List<Question>> questionPool) {
        this.questionPool = questionPool;
    }
    
    @Override
    public List<Question> createQuestionSet() {

        if( questionPool.size() < Rules.DEFAULT.getQuestionsPerGame() )
            throw new IllegalStateException("Not enough questions for a new game set. Questions: " + questionPool.size());
        
        List<Question> questionSet = new ArrayList<>();
        
        Random rand = new Random();
        
        int questions = 0;
        for (Integer category : questionPool.keySet()) {
            List<Question> questionsForCategory = questionPool.get(category);
            
            int index = rand.nextInt(questionsForCategory.size());
            
            Question q = questionsForCategory.get(index).clone();
            q.shuffleAnswers();
            questionSet.add( q );
            
            if( ++questions == Rules.DEFAULT.getQuestionsPerGame() ) {
                break;
            }
        }
        
        return questionSet;        
    }
    
    @Override
    public String toString() {
        
        StringBuilder sb = new StringBuilder();
        
        sb.append("[");
        
        for (Integer category : questionPool.keySet()) {
            sb.append(category).append("=");
            sb.append(questionPool.get(category).size());
            sb.append(", ");
        }
        
        if(sb.toString().endsWith(", "))
            sb.replace(sb.length()-2, sb.length(), "");
        
        sb.append("]");
        
        return sb.toString();
    }
    
}
