package wwm.data;

public class BadDatabaseException extends RuntimeException {

    public BadDatabaseException(String message) {
        super(message);
    }
    
}
