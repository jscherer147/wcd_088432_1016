create table questions(
    id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY 
                (START WITH 1, INCREMENT BY 1),
    category int,
    text varchar(300),
    correctAnswer varchar(300),
    wrongAnswer1 varchar(300),
    wrongAnswer2 varchar(300),
    wrongAnswer3 varchar(300)
)
