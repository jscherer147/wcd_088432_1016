create table questions(
    id integer primary key autoincrement not null,
    category int,
    text varchar(300),
    correctAnswer varchar(300),
    wrongAnswer1 varchar(300),
    wrongAnswer2 varchar(300),
    wrongAnswer3 varchar(300)
)
