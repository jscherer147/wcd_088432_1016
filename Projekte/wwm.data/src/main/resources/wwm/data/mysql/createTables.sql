create table if not exists wwm.questions(
    id integer primary key auto_increment not null,
    category integer,
    text varchar(300),
    correctAnswer varchar(300) CHARACTER SET utf8,
    wrongAnswer1 varchar(300) CHARACTER SET utf8,
    wrongAnswer2 varchar(300) CHARACTER SET utf8,
    wrongAnswer3 varchar(300) CHARACTER SET utf8
);
