package wwm.data.utils;

import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import wwm.Question;

public class CsvParserTest {

    @Test
    public void testParseFromResource() throws Exception {
        System.out.println("--- parseFromResource");
        String resource = "WWM.csv";
        String charSet = "ISO-8859-1";
        Class clazz = CsvParser.class;
        Map<Integer, List<Question>> result = CsvParser.parseFromResource(resource, charSet, clazz);

        int expectedCategories = 15;

        assertEquals("Categories count", expectedCategories, result.size());

        int expectedQuestions = 993;

        int actualQuestions = 0;
        for (List<Question> list : result.values()) {
            actualQuestions += list.size();
        }

        assertEquals("Bad questions number", expectedQuestions, actualQuestions);
    }

}
