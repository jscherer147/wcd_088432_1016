package wwm.data.memory;

import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import wwm.Question;
import wwm.data.GameDAO;
import wwm.data.utils.CsvParser;

public class MemoryDAOTest {
    
    public MemoryDAOTest() {
    }

    @org.junit.Test
    public void testCreateQuestionSet() throws Exception {
        System.out.println("createQuestionSet");
        
        Map<Integer, List<Question>> allQuestions = CsvParser.parseFromResource("WWM.csv", 
                "ISO-8859-1", CsvParser.class);
        GameDAO instance = new MemoryDAO( allQuestions );
        List<Question> result = instance.createQuestionSet();
        
        int expectedSize = 15;
                
        assertEquals("Wrong number of questions", expectedSize, result.size());
    }

    
}
