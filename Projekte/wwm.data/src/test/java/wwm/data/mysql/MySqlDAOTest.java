package wwm.data.mysql;

import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import wwm.Question;
import wwm.Rules;
import wwm.data.utils.CsvParser;

public class MySqlDAOTest {
    
    public MySqlDAOTest() {
    }

   @Before
    public void prepareDb() throws Exception {
        MySqlDAO.dropDatabase();
        MySqlDAO.createDatabase();
        MySqlDAO.createTables();
    }
    
    @Test
    public void testCreateQuestionSet() throws Exception {
        System.out.println("*** Test: createQuestionSet");
        
        Map<Integer, List<Question>> map = CsvParser.parseFromResource("WWM.csv", "ISO-8859-1", CsvParser.class);

        MySqlDAO instance = new MySqlDAO();
        for (List<Question> listQuestions : map.values()) {
            instance.insertQuestions(listQuestions);
        }
        
        List<Question> result = instance.createQuestionSet();
        assertEquals(Rules.DEFAULT.getQuestionsPerGame(), result.size());
    }


}
