package wwm.data.derby;

import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import wwm.Question;
import wwm.Rules;
import wwm.data.utils.CsvParser;

public class DerbyDAOTest {

    static DerbyDAO dao;

    @Before
    public void setUp() throws Exception {
        dao = new DerbyDAO("wwmdb");
        dao.dropDatabase();
        dao.createDatabase();
    }

    @Test
    public void testCreateQuestionSet() throws Exception {

        Map<Integer, List<Question>> map = CsvParser.parseFromResource("WWM.csv", "ISO-8859-1", CsvParser.class);

        for (List<Question> listQuestions : map.values()) {
            dao.insertQuestions(listQuestions);
        }

        List<Question> result = dao.createQuestionSet();
        assertEquals(Rules.DEFAULT.getQuestionsPerGame(), result.size());
    }

    @Test
    public void testCreateDatabase() throws Exception {
    }

    @Test
    public void testDropDatabase() throws Exception {
    }

    @Test
    public void testInsertQuestions() throws Exception {
    }

}
