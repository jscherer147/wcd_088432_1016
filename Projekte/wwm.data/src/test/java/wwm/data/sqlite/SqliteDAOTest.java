package wwm.data.sqlite;

import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import wwm.Question;
import wwm.Rules;
import wwm.data.utils.CsvParser;

/**
 *
 * @author apatrin
 */
public class SqliteDAOTest {

    private static SqliteDAO dao;
    
    public SqliteDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception {
        dao = new SqliteDAO("C:\\wwm.sqlite");
        dao.dropDatabase();
        dao.createDatabase();
    }

    @Test
    public void testCreateQuestionSet() throws Exception {
        
        Map<Integer, List<Question>> map = CsvParser.parseFromResource("WWM.csv", "ISO-8859-1", CsvParser.class);

        for (List<Question> listQuestions : map.values()) {
            dao.insertQuestions(listQuestions);
        }

        List<Question> result = dao.createQuestionSet();
        assertEquals(Rules.DEFAULT.getQuestionsPerGame(), result.size());        
        
    }
    
}
